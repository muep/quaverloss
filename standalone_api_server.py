#!/usr/bin/env python
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import wsgiref.simple_server

import quaverloss.web.api

def argument_parser():
    p = argparse.ArgumentParser()

    p.add_argument("config_path")
    p.add_argument("port", type=int)

    return p

def main():
    pargs = argument_parser().parse_args()

    config = quaverloss.web.api.config_dict(pargs.config_path)
    app = quaverloss.web.api.wsgi_application(config)

    server = wsgiref.simple_server.make_server(
        "0.0.0.0",
        pargs.port,
        app
    )
    server.serve_forever()

if __name__ == "__main__":
    main()
