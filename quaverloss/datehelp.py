# quaverloss/datehelp.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import datetime
import sys
import time

if sys.version_info >= (3, 0):
    json_type = "application/json"
    utc = datetime.timezone.utc
    epoch = datetime.datetime(1970, 1, 1, tzinfo = utc)
else:
    # Python 2
    json_type = b"application/json"

    # Custome tzinfo implementation that looks like
    # datetime.timezone.utc from Python 3.
    class UtcTzInfo(datetime.tzinfo):
        def dst(self, dt):
            return datetime.timedelta(0)

        def utcoffset(self, dt):
            return datetime.timedelta(0)

        def tzname(self, dt):
            return "UTC+00:00"

    utc = UtcTzInfo()
    epoch = datetime.datetime(1970, 1, 1, tzinfo = utc)

def stamp_to_datetime(stamp):
    return epoch + datetime.timedelta(seconds=stamp)

def stamp_from_datetime(stamp):
    return int ((stamp - epoch).total_seconds())

def stamp_to_datestr(stamp):
    offset = datetime.timedelta(seconds=time.timezone)
    ndatetime = stamp_to_datetime(stamp)
    ldatetime = ndatetime - offset
    return ldatetime.strftime("%Y-%m-%d %H:%M")
