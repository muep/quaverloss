/*
 * quaverloss/web/client/index.js
 *
 * Copyright 2013 Joonas Sarajärvi
 *
 * This file is part of Quaverloss.
 *
 * Quaverloss is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Quaverloss is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Quaverloss. In source copies of Quaverloss, it is
 * usually included in files named COPYING and COPYING.LESSER. The
 * license is also available at <http://www.gnu.org/licenses/>.
 */


/*
 * Miscellaneous standalone-ish functions
 */
function date_text_from_posix_stamp(posix_stamp) {
    var d = new Date(posix_stamp * 1000);

    var year = "" + d.getFullYear();
    var month = "" + (d.getMonth() + 1);
    var day = "" + d.getDate();

    if (month.length < 2) {
        month = "0" + month;
    }

    if (day.length < 2) {
        day = "0" + day;
    }

    return year + "-" + month + "-" + day;
}

function do_nothing() {}

function empty_div() {
    return $("<div></div>");
}

function ifocus_from_topsel(select_element) {
    var f = select_element.val();
    var f_num = parseInt(f, 10);
    return isNaN(f_num) ? null : f_num;
}

/* Global state. */
function make_application() {
    var instrument_focus_select = $("#instrument_focus_select");

    var app = {
        update_instruments: do_nothing,
        update_success_levels: do_nothing,
        instruments: [],
        instrument_focus: ifocus_from_topsel(instrument_focus_select),
        success_levels: []
    };

    /* Hook up the global UI items to pass this specific
     * application object to views */
    $("#navbar_tunes").off("click").on("click", function() {
        set_dynmode(tunes_page(app));
    });
    $("#navbar_instruments").off("click").on("click", function() {
        set_dynmode(instruments_page(app));
    });
    $("#navbar_practice").off("click").on("click", function() {
        set_dynmode(suggestions_page(app));
    });
    $("#navbar_account").off("click").on("click", function() {
        set_dynmode(account_page());
    });

    instrument_focus_select.off("change").on("change", function() {
        app.instrument_focus = ifocus_from_topsel(instrument_focus_select);
        app.update_instruments();
    });

    return app;
}

function set_dynmode(thing) {
    var dm = $("#dynamic_content");
    dm.empty();
    dm.append(thing);
}

function check_instruments(app) {
    $.ajax({
        url: "../api/instruments",
        type: "GET",
        dataType : "json",

        success: function(json) {
            /* Refill the instrument focus selector that is near the
             * top of the page. */
            var select = $("#instrument_focus_select");
            var div = $("#instrument_focus_div");
            select.empty();
            select.append($("<option></option>").text("Any"));

            $.each(json, function(index, instrument) {
                var option = $("<option></option>")
                    .text(instrument["instrument_name"])
                    .val(instrument["instrument_id"]);
                select.append(option);
            });

            if (json.length >= 2) {
                div.show();
            } else {
                div.hide();
            }

            app.instruments = json;
            app.update_instruments();
        }
    });
}

function check_successlevels(app) {
    $.ajax({
        url: "../api/success_levels",
        type: "GET",
        dataType : "json",

        success: function(json) {
            app.success_levels = json;
            app.update_success_levels();
        }
    });
}

function check_login() {
    $.ajax({
        url: "../api/login",
        type: "GET",
        dataType: "json",

        success: function(json) {
            var username = json["username"];
            if ("" == username) {
                /* Not logged in, so show the login stuff */
                $(".only_logoutd").show();
                $(".only_logind").hide();
                set_dynmode(login_signin_page());
            } else {
                /* Successfully logged in, so hide login
                 * stuff, fetch some data and show some UI stuff */
                $(".username_span").text(username);
                $(".only_logoutd").hide();
                $(".only_logind").show();

                /* This object is intended to get passed around
                 * between views that need access to application-global
                 * state. */
                var app = make_application();
                set_dynmode(tunes_page(app));
                check_instruments(app);
                check_successlevels(app);
            }
        }
    });
}

function login_div() {
    function handle_success() {
        check_login();
    }

    function handle_failure(xhr, failure) {
        alert("Could not log in");
        set_dynmode(login_signin_page());
    }

    var username_input = $('<input type="text" />');
    var password_input = $('<input type="password" />');
    var login_button = $('<input type="button" value="Log in" />');

    function send_login() {
        var data = {
            username: username_input.val(),
            password: password_input.val()
        };

        $.ajax({
            url: "../api/login",
            type: "POST",
            data: data,
            success: handle_success,
            error: handle_failure
        });
    };

    login_button.click(send_login);

    var username_div = empty_div().text("Username:");
    username_div.append(username_input);

    var password_div = empty_div().text("Password:");
    password_div.append(password_input);

    var div = empty_div();
    div.append(username_div);
    div.append(password_div);
    div.append(login_button);
    return div;
}

function signup_with(username_input, pw0_input, pw1_input) {
    var username = username_input.val();
    var pw0 = pw0_input.val();
    var pw1 = pw1_input.val();

    if (username.length == 0) {
        alert("User name can not be empty.");
        return;
    }

    if (pw0.length == 0) {
        alert("Password can not be empty.");
        return;
    }

    if (pw0 != pw1) {
        alert("Passwords must be identical.");
        return;
    }

    $.ajax({
        url: "../api/signup",
        type: "POST",
        data: {
            username: username,
            password: pw0
        },
        success: function(response) {
            window.alert("Account " + username + " was created for you.");
            set_dynmode(login_signin_page());
        },
        error: function(xhr, status) {
            window.alert("Account " + username + " could not be created.");
        }
    });
}

function signup_div() {
    var username_input = $('<input type="text" />');
    var password0_input = $('<input type="password" />');
    var password1_input = $('<input type="password" />');
    var signup_button = $('<input type="button" value="Sign up" />');

    signup_button.click(function() {
        signup_with(username_input, password0_input, password1_input);
    });

    var username_div = empty_div().text("Username:");
    username_div.append(username_input);
    var password0_div = empty_div().text("Password:");
    password0_div.append(password0_input);
    var password1_div = empty_div().text("Retype password:");
    password1_div.append(password1_input);

    var div = empty_div();
    div.append(username_div);
    div.append(password0_div);
    div.append(password1_div);
    div.append(signup_button);
    return div;
}

function login_signin_page() {
    var page = empty_div();
    page.append($("<h2>Log in</h2>"));
    page.append(login_div());
    page.append($("<h2>Create an account</h2>"));
    page.append(signup_div());
    return page;
}

function send_logout() {
    $.ajax({
        url: "../api/logout",
        type: "POST",
        complete: check_login
    });
}

function instrument_by_id(instrument_id, instruments) {
    for (var n = 0; n < instruments.length; ++n) {
        var instrument = instruments[n];
        if (instrument["instrument_id"] === instrument_id) {
            return instrument;
        }
    }
    return null;
}

function instrument_detail_page(app, instrument_id) {
    var instrument = instrument_by_id(instrument_id, app.instruments);

    var title = $("<h2>Instrument details</h2>");

    var name_div = empty_div();
    name_div.append("Name: ");

    /* TODO: Add editing functionality */
    var name_value = instrument["instrument_name"];
    name_div.append(name_value);

    var data_div = empty_div();
    data_div.append(name_div);

    var page = empty_div();
    page.append(title);
    page.append(data_div);
    return page;
}

function instrument_link(app, instrument_id, instrument_name) {
    var span = $('<span class="linkish"></span>');
    span.text(instrument_name);
    span.on("click", function() {
        set_dynmode(instrument_detail_page(app, instrument_id));
    });
    return span;
}

function instruments_page(app) {
    var title = $("<h2>Instruments</h2>");

    var instuments_div = empty_div();
    $.each(app.instruments, function(n, instrument) {
        var i_num = instrument["instrument_id"];
        var i_nom = instrument["instrument_name"];
        var entry = empty_div();
        entry.append(instrument_link(app, i_num, i_nom));
        instuments_div.append(entry);
    });

    var page = empty_div();
    page.append(title);
    page.append(instuments_div);
    return page;
}

function practice_list_entry(app, practice) {
    var stamp = practice["practice_stamp"];
    var date_text = date_text_from_posix_stamp(stamp);
    var practice_session_id = practice["practice_session_id"];

    var text =
        date_text + ": " +
        " with " +
        practice["instrument_name"];

    var title_div = $("<div></div>")
        .text(text)
        .click(function() {
            set_dynmode(practice_details_page(app, practice_session_id));
        })
        .addClass("practice_list_title");

    var item = $("<div></div>");
    item.append(title_div);

    return item;
}

/*
 * Select the instrument for newly added practices.
 *
 * This is used from tune_detail_page.
 *
 * The rules are fairly complex so it is nice to have
 * them in a separate function.
 */
function id_of_selected_instrument(app, instrument_select) {
    if (app.instruments.length == 0) {
        /* The caller should handle this error somehow. And likely
         * the application should not end up in a state where
         * no instruments are defined. */
        return null;
    }

    if (app.instruments.length == 1) {
        return app.instruments[0]["instrument_id"];
    }

    function has_instrument(potential_id) {
        for (var n = 0; n < app.instruments.length; ++n) {
            var cur = app.instruments[n]["instrument_id"];
            if (cur === potential_id) {
                return true;
            }
        }
        return false;
    }

    var focused_id = app.instrument_focus;
    if (has_instrument(focused_id)) {
        return focused_id;
    }

    var selected_id = parseInt(instrument_select.val(), 10);
    if (has_instrument(selected_id)) {
        return selected_id;
    }

    /* If we end up here, it likely means that the instrument
     * related stuff in app had somehow fallen out of sync with
     * the UI elements. */
    return null;
}

/* Construct the tune details view */
function tune_detail_page(app, tune_id) {
    var title_heading = $("<h2></h2>");

    var uris_heading = $("<h3>Available at</h3>");
    var uris_list = $("<ul></ul>");

    var uris_div = empty_div();
    uris_div.append(uris_heading);
    uris_div.append(uris_list);

    var uri_add_field = $('<input type="text" />');
    var uri_add_button = $('<input type="button" value="Add" />')
        .on("click", function() {
            var uri = uri_add_field.val();
            if (!(uri.length > 0)) {
                alert("Can not add an empty URI");
                return;
            }

            $.ajax({
                url: "../api/tune_uri_add",
                type: "POST",
                data: {
                    tune_id: tune_id,
                    uri: uri
                },
                success: function() {
                    uri_add_field.val("");
                    fetch_details();
                }
            });
        });

    var uri_add_form = $("<form></form>");
    uri_add_form.append("Add a new URI for this tune: ");
    uri_add_form.append(uri_add_field);
    uri_add_form.append(uri_add_button);

    var practices_div = empty_div();

    var instrument_select = $("<select></select>")
    var instrument_select_div = empty_div()
        .text("Instrument: ");
    instrument_select_div.append(instrument_select);

    /* This function will update the UI of this page
     * to match the instrument-related global state. */
    function update_instruments() {
        if (app.instrument_focus != null) {
            instrument_select_div.hide();
        } else if (app.instruments.length < 2) {
            instrument_select_div.hide();
        } else {
            instrument_select.empty();
            $.each(app.instruments, function(n, instrument) {
                var option = $("<option></option>")
                    .text(instrument["instrument_name"])
                    .val(instrument["instrument_id"]);
                instrument_select.append(option);
            });
            instrument_select_div.show();
        }
        fetch_details();
    }

    /* Arrange it to be called also when check_instruments
     * changes it. */
    app.update_instruments = update_instruments;

    var success_level_select = $("<select></select>");
    $.each(app.success_levels, function(n, sl) {
        var option = $("<option></option>")
            .text(sl["name"])
            .val(sl["success_level_id"]);
        success_level_select.append(option);
    });
    var success_level_select_div = empty_div()
        .text("Success level: ");
    success_level_select_div.append(success_level_select);
    if (app.success_levels.length < 2) {
        success_level_select_div.hide();
    }

    var remarks_field = $("<textarea></textarea>");
    var remarks_div = empty_div();
    remarks_div.append($("<div>Remarks:</div>"));
    remarks_div.append(remarks_field);

    var add_practice_button =
        $('<input type="button" value="Submit"></input>');
    add_practice_button.on("click", function() {
        var instrument_id = id_of_selected_instrument(app,
                                                      instrument_select);

        if (instrument_id == null) {
            alert("Failed to determine the selected instrument");
            return;
        }

        var remarks = remarks_field.val();

        var success_lvl_id = success_level_select.val();

        var data = {
            tune_id: tune_id,
            instrument_id: instrument_id,
            remarks: remarks,
            success_level_id: success_lvl_id
        };
        $.ajax({
            url: "../api/practice_add",
            method: "POST",
            data: data,
            success: function() {
                fetch_details();
            }
        });
    });

    var add_practice_div = empty_div();
    add_practice_div.hide();

    add_practice_div.append($("<h3>Practiced this just now</h3>"));
    add_practice_div.append(instrument_select_div);
    add_practice_div.append(success_level_select_div);
    add_practice_div.append(remarks_div);
    add_practice_div.append(add_practice_button);

    function fetch_details() {
        var data = {
            practice_count: 5,
            tune_id: tune_id
        };
        if (app.instrument_focus != null) {
            data.instrument_id = app.instrument_focus;
        }
        $.ajax({
            url: "../api/tune_details",
            type: "GET",
            data: data,
            dataType: "json",
            success: function (json) {
                title_heading.text(json["name"]);

                practices_div.empty();
                var practices = json["recent_practices"];
                if (practices.length > 0) {
                    practices_div.append($("<h3>Recent practices</h3>"));
                }
                $.each(practices, function(n, psession) {
                    var entry = practice_list_entry(app, psession)
                    practices_div.append(entry);
                });

                var uris = json["uris"];
                uris_list.empty();
                $.each(uris, function(n, uri) {
                    var a = $("<a></a>")
                        .text(uri)
                        .attr("href", uri);

                    var span = $("<span>(remove)</span>")
                        .addClass("linkish")
                        .on("click", function() {
                            /* Are we having enough nested functions
                             * already? */
                            $.ajax({
                                url: "../api/tune_uri_rm",
                                type: "POST",
                                data: {
                                    tune_id: tune_id,
                                    uri: uri
                                },
                                success: fetch_details
                            });
                        });
                    var li = $("<li></li>");

                    li.append(a);
                    li.append("  ");
                    li.append(span);
                    uris_list.append(li);
                });
                if (uris.length > 0) {
                    uris_div.show();
                } else {
                    uris_div.hide();
                }

                add_practice_div.show();
            }
        });
    }
    /* Invoke this directly to get the initial setup.
     * It will also call fetch_details */
    update_instruments();

    var rm_button = $('<input type="button" value="Remove"></input>');
    rm_button.on("click", function() {
        if (!window.confirm("Really remove a tune?")) {
            return;
        }

        $.ajax({
            url: "../api/tune_rm",
            type: "POST",
            data: { tune_id : tune_id },
            success: function() {
                set_dynmode(tunes_page(app));
            }
        });
    });

    var rm_div = empty_div();
    rm_div.append($("<h3>Remove this tune</h3>"));
    rm_div.append(rm_button);

    var page = empty_div();
    page.append(title_heading);
    page.append(uris_div);
    page.append(uri_add_form);
    page.append(practices_div);
    page.append(add_practice_div);
    page.append(rm_div);
    return page;
}

function tune_link(app, tune_id, tune_name) {
    var span = $('<span class="linkish"></span>');
    span.text(tune_name);
    span.on("click", function() {
        set_dynmode(tune_detail_page(app, tune_id));
    });
    return span;
};

function tunes_page(app) {
    var search_field = $('<input type="text" />');
    var search_button = $('<input type="button" value="Search" />');
    var add_button = $('<input type="button" value="Add" />');

    var result_count_div = empty_div();
    var result_list_div = empty_div();

    function receive_search(json) {
        result_count_div.text(json.length + " tunes:");

        result_list_div.empty();
        $.each(json, function(n, tune) {
            var element = empty_div();
            var link = tune_link(app, tune.tune_id, tune.tune_name);
            element.append(link);
            result_list_div.append(element);
        });
    };

    function search() {
        var search_expr = search_field.val();

        var data = {
            tune_name: search_expr
        };

        $.ajax({
            url: "../api/tune_search",
            type: "GET",
            dataType: "json",
            data: data,
            success: receive_search
        });
    }

    function add() {
        var data = {
            tune_name: search_field.val()
        };

        $.ajax({
            url: "../api/tune_add",
            type: "POST",
            data: data,
            success: search
        });
    }

    search_button.click(search);
    add_button.click(add);

    var search_div = empty_div().text("Tune name: ");
    search_div.append(search_field);
    search_div.append(search_button);
    search_div.append(add_button);

    var page = empty_div();
    page.append($("<h2>Tune search</h2>"));
    page.append(search_div);
    page.append(result_count_div);
    page.append(result_list_div);
    return page;
}


function suggestions_page(app) {
    var recent_div = empty_div();
    var neglect_div = empty_div();

    function refetch() {
        var instrument_id = $("#instrument_focus_select").val();

        data = {};
        if ((instrument_id != null) && (instrument_id != "Any")) {
            data["instrument_id"] = instrument_id;
        }

        $.ajax({
            url: "../api/practice_suggestions",
            data: data,
            type: "GET",
            dataType: "json",
            success: function(json) {
                recent_div.empty();
                $.each(json["practiced"], function(idx, tune) {
                    var l = tune_link(app, tune["tune_id"], tune["tune_name"]);
                    var d = empty_div();
                    d.append(l);

                    recent_div.append(d);
                });

                neglect_div.empty();
                $.each(json["neglected"], function(idx, tune) {
                    var l = tune_link(app, tune["tune_id"], tune["tune_name"]);
                    var d = empty_div();
                    d.append(l);

                    neglect_div.append(d);
                });
            }
        });
    }

    var page = empty_div();
    page.append($("<h3>Recently practiced</h3>"));
    page.append(recent_div);
    page.append($("<h3>Recently neglected</h3>"));
    page.append(neglect_div);

    app.update_instruments = refetch;
    refetch();

    return page;
}

function account_page_import_div() {
    var file_input = $("<input></input>")
        .attr("name", "datafile")
        .attr("type", "file");

    var submit_button = $("<input></input>")
        .attr("type", "submit");

    var form = $("<form></form>")
        .attr("enctype", "multipart/form-data")
        .attr("action", "../api/import")
        .attr("method", "post")
        .text("File: ");

    form.append(file_input);
    form.append(submit_button);

    var div = empty_div();
    div.append($("<h3>Import data</h3>"));
    div.append(form);

    function setup_submit() {
        var file_value = file_input.val();
        if (file_value !== undefined && file_value.length > 0) {
            submit_button.attr("disabled", false);
            submit_button.val("Import");
        } else {
            submit_button.attr("disabled", true);
            submit_button.val("File not set");
        }
    };

    file_input.on("change", setup_submit);
    setup_submit();

    return div;
}

function account_page_passwd_div() {
    var oldpw_input = $('<input type="password" />');
    var pw0_input = $('<input type="password" />');
    var pw1_input = $('<input type="password" />');
    var setpw_button = $('<input type="button" value="Set password" />');
    setpw_button.on("click", function() {
        var new_password = pw0_input.val();
        if (new_password !== pw1_input.val()) {
            alert("The two copies of the new password are not identical");
            return;
        }

        var old_password = oldpw_input.val();

        $.ajax({
            url: "../api/passwd",
            type: "POST",
            data: {
                old_password: old_password,
                new_password: new_password
            },
            success: function() {
                alert("Password change succeeded");
                oldpw_input.val("");
                pw0_input.val("");
                pw1_input.val("");
            },
            error: function() {
                alert("Password change failed");
                oldpw_input.val("");
                pw0_input.val("");
                pw1_input.val("");
            }
        });
    });

    var oldpw_div = empty_div();
    oldpw_div.append("Old password: ");
    oldpw_div.append(oldpw_input);

    var pw0_div = empty_div();
    pw0_div.append("New password: ");
    pw0_div.append(pw0_input);

    var pw1_div = empty_div();
    pw1_div.append("Retype new password: ");
    pw1_div.append(pw1_input);

    var form = $("<form />");
    form.append(oldpw_div);
    form.append(pw0_div);
    form.append(pw1_div);
    form.append(setpw_button);

    var title = $("<h3>Change password</h3>");

    var div = empty_div();
    div.append(title);
    div.append(form);
    return div;
}

function account_info_div() {
    var schema_version_now_span = $("<span />");
    var schema_version_max_span = $("<span />");
    var upgrade_button = $('<input type="button" value="Upgrade" />');
    var upgrade_div = empty_div();
    upgrade_div.append("Upgrade to the latest schema version: ");
    upgrade_div.append(upgrade_button);
    upgrade_div.hide();

    function fetch_info() {
        $.ajax({
            url: "../api/account_info",
            type: "GET",
            dataType: "json",
            success: function(json) {
                var sv_max = json["schema_version_max"];
                var sv_now = json["schema_version_now"];
                schema_version_max_span.text(sv_max);
                schema_version_now_span.text(sv_now);
                if (sv_max > sv_now) {
                    upgrade_div.show();
                } else {
                    upgrade_div.hide();
                }
            }
        });
    }

    function upgrade() {
        $.ajax({
            url: "../api/upgrade_db",
            type: "POST",
            success: fetch_info
        });
    }
    upgrade_button.on("click", upgrade);

    var sv_now_div = empty_div();
    sv_now_div.append("Current database schema version: ");
    sv_now_div.append(schema_version_now_span);

    var sv_max_div = empty_div();
    sv_max_div.append("Latest supported schema version: ");
    sv_max_div.append(schema_version_max_span);

    fetch_info();

    var title = $("<h3>Database status</h3>")
    var div = empty_div();
    div.append(title);
    div.append(sv_now_div);
    div.append(sv_max_div);
    div.append(upgrade_div);
    return div;
}

function account_page() {
    var import_div = account_page_import_div();
    var passwd_div = account_page_passwd_div();
    var acinfo_div = account_info_div();

    var page = empty_div();
    page.append(import_div);
    page.append(passwd_div);
    page.append(acinfo_div);
    return page;
}

function practice_details_page(app, practice_session_id) {
    var title = $("<h2>Practice session details</h2>");
    var txt = $("<p></p>");

    var page = empty_div();

    page.append(title);
    page.append(txt);

    function remove_and_to_tunedets(practice_session_id, tune_id) {
        $.ajax({
            url: "../api/practice_rm",
            type: "POST",
            data: { "practice_session_id" : practice_session_id },
            success: function() {
                set_dynmode(tune_detail_page(app, tune_id));
            }
        });
    }

    $.ajax({
        url: "../api/practice_details",
        data: { "practice_session_id" : practice_session_id },
        type: "GET",
        dataType: "json",
        success: function(json) {
            var tune_id = json["tune_id"];
            var tune_name = json["tune_name"];
            var instrument_id = json["instrument_id"];
            var instrument_name = json["instrument_name"];
            var remarks = json["practice_remarks"];
            var stamp = json["practice_stamp"];
            var date_text = date_text_from_posix_stamp(stamp);

            txt.append("On ");
            txt.append(date_text);
            txt.append(", played ");
            txt.append(tune_link(app, tune_id, tune_name));
            txt.append(" with ");
            txt.append(instrument_link(app, instrument_id, instrument_name));

            if (remarks.length > 0) {
                page.append($("<h3>Remarks</h3>"));
                page.append($("<p></p>").text(remarks));
            }

            page.append($("<h3>Actions</h3>"));
            var rm_button = $('<input type="button"></input>');
            rm_button.val("Remove");
            rm_button.on("click", function() {
                if (!window.confirm("Really remove a practice session?")) {
                    return;
                }

                remove_and_to_tunedets(practice_session_id, tune_id);
            });
            page.append(rm_button);
       }
    });

    return page;
}

function init() {
    $("#logout_button").click(send_logout);

    /* Initially show just empty page, but then immediately
     * start the login check that will load some content. */
    set_dynmode(empty_div());
    check_login();
}

$(document).ready(init);
