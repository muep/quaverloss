# quaverloss/web/api.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

# python standard library imports
try:
    import configparser
except ImportError:
    # python2
    import ConfigParser as configparser

import json
import os
import os.path
import sqlite3
import sys
import time

# pyramid imports
import pyramid.config
import pyramid.httpexceptions
import pyramid.response
import pyramid.session

# quaverloss imports
import quaverloss.db.config as qdb_config
import quaverloss.db.instruments as qdb_instruments
import quaverloss.db.maintain as qdb_maintain
import quaverloss.db.practicesessions as qdb_practicesessions
import quaverloss.db.successlevels as qdb_successlevels
import quaverloss.db.tunes as qdb_tunes
import quaverloss.c11y as c11y
import quaverloss.dbhelp as dbhelp

if sys.version_info >= (3, 0):
    # python3
    json_type = "application/json"
else:
    # python2
    json_type = b"application/json"

def int_or_none(sth):
    if sth == None:
        return None

    return int(sth)

# Converts None to current time as an integer, and otherwise
# passes an int through.
def timestamp_or_now(sth):
    if sth == None:
        return int(time.time())

    return int(sth)

##
# Make a view function that can be passed to pyramid.
#
# @param config is a dictionary that describes the runtime config
# that was passed from outside.
#
# @param inner_view is a function that takes:
# - the application config as its first parameter
# - the request from a client as its second parameter.
def view(config, inner_view):
    # A function that pyramid can use as a view.
    def real_view(request):
        return inner_view(config, request)

    return real_view

##
# Make a pyramid-compatible view that requires a logged-in user
#
# @param config is a dictionary that describes the runtime config
# that was passed from outside.
#
# @param inner_view is a function that takes:
# - the database connection as its first parameter
# - the application config as the second parameter
# - the user account name of the user as its third parameter
# - the request from client as its fourth parameter
def loginny_view(config, inner_view):
    def real_view(request):
        username = request.session.get("username", None)
        if username == None:
            return pyramid.httpexceptions.HTTPUnauthorized()

        dbdir = config["database_dir"]

        db_filename = "{}/{}/quaverloss.sqlite3".format(dbdir, username)
        db_filename = os.path.normpath(db_filename)

        if not os.path.isfile(db_filename):
            return pyramid.httpexceptions.HTTPForbidden()

        db = sqlite3.connect(db_filename)
        db.execute("PRAGMA foreign_keys = ON;")
        try:
            result = inner_view(db, config, username, request)
            db.commit()
            return result
        finally:
            db.close()

    return real_view

def login(config, request):
    if request.method != "POST":
        username = request.session.get("username", None)

        if username == None:
            txt = json.dumps({ "username" : "" })
        else:
            txt = json.dumps({ "username" : username })

        return pyramid.response.Response(txt, content_type=json_type)

    try:
        username = request.POST["username"]
        password = request.POST["password"]
    except Exception:
        return pyramid.httpexceptions.HTTPForbidden()

    dbdir = config["database_dir"]
    db_filename = "{}/{}/quaverloss.sqlite3".format(dbdir, username)
    db_filename = os.path.normpath(db_filename)

    if not os.path.isfile(db_filename):
        return pyramid.httpexceptions.HTTPForbidden()

    db = sqlite3.connect(db_filename)
    try:
        stored_hash = qdb_config.get(db, "password")
    finally:
        db.close()

    if stored_hash == "":
        return pyramid.httpexceptions.HTTPForbidden()

    if c11y.crypt(password, stored_hash) != stored_hash:
        return pyramid.httpexceptions.HTTPForbidden()

    # Succeeded through all the steps.
    request.session["username"] = username
    txt = json.dumps({"username": username})
    return pyramid.response.Response(txt, content_type=json_type)

def logout(config, request):
    if request.method == "POST":
        request.session.invalidate()
        return pyramid.response.Response()

    return pyramid.httpexceptions.HTTPNotFound()

def _make_alloweds_set():
    alphas = "abcdefghijklmnopqrstuvwxyz"
    nums = "0123456789"
    extras = "_-"
    return set(alphas + alphas.upper() + nums + extras)

_alloweds_set = _make_alloweds_set()

def _is_sane_username(txt):
    # This set of requirements is pretty arbitrary, but likely
    # names conforming to these should not cause big problems

    if len(txt) < 4:
        return False

    if len(txt) > 30:
        return False

    if not all(map(lambda c: c in _alloweds_set, txt)):
        return False

    return True

def signup(config, request):
    try:
        username = request.POST["username"]
        password = request.POST["password"]
    except Exception:
        return pyramid.httpexceptions.HTTPNotFound()

    if not _is_sane_username(username):
        txt = "Username is not acceptable."
        return pyramid.httpexceptions.HTTPNotFound(txt)

    dbdir = config["database_dir"]
    user_dir = os.path.normpath("{}/{}".format(dbdir, username))

    if os.path.exists(user_dir):
        txt = "Username is already taken"
        return pyramid.httpexceptions.HTTPNotFound(txt)

    os.makedirs(user_dir)

    db_filename = "{}/quaverloss.sqlite3".format(user_dir)

    db = dbhelp.open_db(db_filename)
    try:
        qdb_config.set(db, "password", c11y.crypt(password))

        # Some default content for new accounts. With no instruments
        # and no success levels, the thing kind of does not work right.
        #
        # TODO: decide if this could be handled somehow prettier.
        qdb_instruments.add(db, "default")
        qdb_successlevels.add(db, "default", 100, "")
        db.commit()
    finally:
        db.close()

    return pyramid.response.Response()

def passwd(db, config, username, request):
    try:
        old_password = request.POST["old_password"]
        new_password = request.POST["new_password"]
    except Exception:
        return pyramid.httpexceptions.HTTPNotFound()

    old_hash = qdb_config.get(db, "password")
    if c11y.crypt(old_password, old_hash) != old_hash:
        txt = "Old password was not correct"
        return pyramid.httpexceptions.HTTPForbidden(txt)

    new_hash = c11y.crypt(new_password)
    qdb_config.set(db, "password", new_hash)
    return pyramid.response.Response()

# Import a quaverloss database file for an user.
#
# This will currently *replace* all previous database
# content stored for the user whose account is imported to.
# Only the password is preserved from the old database.
#
# The whole function is currently a bit cumbersome and
# ugly, so it might make sense to revise this at some point.
def import_db(config, request):
    username = request.session.get("username", None)
    if username == None:
        return pyramid.httpexceptions.HTTPUnauthorized()

    dbdir = config["database_dir"]

    db_filename = "{}/{}/quaverloss.sqlite3".format(dbdir, username)
    db_filename = os.path.normpath(db_filename)

    tmp_filename = "{}/{}/imported_tmp.sqlite3".format(dbdir, username)
    tmp_filename = os.path.normpath(tmp_filename)

    if not os.path.isfile(db_filename):
        return pyramid.httpexceptions.HTTPForbidden()

    # Extract the old password hash from the previous database
    # file.
    db = sqlite3.connect(db_filename)
    try:
        old_password = qdb_config.get(db, "password")
    except Exception as e:
        # Did not yet write anything, so should be safe to just return
        # here.
        return pyramid.httpexceptions.HTTPNotFound()
    finally:
        db.close()

    if len(old_password) == 0:
        return pyramid.httpexceptions.HTTPNotFound("Must have password")

    try:
        data = request.POST["datafile"]
    except Exception:
        # Still haven't written anything.
        return pyramid.httpexceptions.HTTPNotFound()

    # Now let's start writing.
    tmp_file = open(tmp_filename, "wb")
    try:
        # Read the whole thing into a local file.
        chunk_size = 1024
        while True:
            chunk = data.file.read(chunk_size)
            if not chunk:
                break
            tmp_file.write(chunk)
        tmp_file.close()

        # Ok, done with reading. Then copy over the password entry.
        db = sqlite3.connect(tmp_filename)
        try:
            qdb_config.set(db, "password", old_password)
            db.commit()
        finally:
            db.close()

        # Succeeded in updating. Let's entirely replace
        # the old database with a new one.
        os.rename(tmp_filename, db_filename)

        # Return a response that redirects to the client page.
        #
        # NOTE: not entirely sure if this is a good solution for
        # clients other than the browser-based one.
        return pyramid.httpexceptions.HTTPFound("../client/")
    except Exception:
        # Going to fail, but let's clean up a bit
        tmp_file.close()
        os.remove(tmp_filename)
        raise

def upgrade_db(db, config, username, request):
    if request.method != "POST":
        return pyramid.httpexceptions.HTTPNotFound()

    qdb_maintain.upgrade(db)
    return pyramid.response.Response()

def account_info(db, config, username, request):
    data = {
        "username": username,
        "schema_version_max": qdb_maintain.schema_version,
        "schema_version_now": qdb_maintain.schema_version_in_db(db),
    }
    body = json.dumps(data)
    return pyramid.response.Response(body, content_type=json_type)

def tune_search(db, config, username, request):
    tune_name = request.params["tune_name"]

    results = qdb_tunes.search_by_name(db, tune_name)
    body = json.dumps(results)
    return pyramid.response.Response(body, content_type=json_type)

def tune_details(db, config, username, request):
    try:
        tune_id = int(request.params["tune_id"])
        instrument_id = int_or_none(request.params.get("instrument_id", None))
        practice_count = int(request.params.get("practice_count", 0))

    except Exception:
        return pyramid.httpexceptions.HTTPNotFound()

    details = qdb_tunes.details(db, tune_id)
    if practice_count == 0:
        recents = []
    else:
        recents = qdb_practicesessions.recents(db,
                                               tune=tune_id,
                                               instrument=instrument_id,
                                               maxCount=practice_count)

    details["recent_practices"] = recents

    body = json.dumps(details)
    return pyramid.response.Response(body, content_type=json_type)

def tune_add(db, config, username, request):
    try:
        # Intentionally accepting parameters just from POST
        tune_name = request.POST["tune_name"]
        uris = request.POST.getall(["uri"])
    except Exception:
        return pyramid.httpexceptions.HTTPNotFound()

    try:
        qdb_tunes.add(db, tune_name, uris=uris)
    except Exception as e:
        return pyramid.httpexceptions.HTTPNotFound(e)
    return pyramid.response.Response()

def tune_rm(db, config, username, request):
    try:
        tune_id = int(request.POST["tune_id"])
    except Exception:
        return pyramid.httpexceptions.HTTPNotFound()

    # Fetch all practice sessions related to this tune.
    # They must be removed or the tune removal will fail.
    for ps in qdb_practicesessions.recents(db, tune=tune_id, maxCount=None):
        qdb_practicesessions.rm(db, ps["practice_session_id"])

    qdb_tunes.rm(db, tune_id)
    return pyramid.response.Response()

def instruments(db, config, username, request):
    results = qdb_instruments.get_all(db)
    body = json.dumps(results)
    return pyramid.response.Response(body, content_type=json_type)

def success_levels(db, config, username, request):
    results = qdb_successlevels.get_all(db)
    body = json.dumps(results)
    return pyramid.response.Response(body, content_type=json_type)

def practice_add(db, config, username, request):
    try:
        tune_id = int(request.POST["tune_id"])
        instrument_id = int(request.POST["instrument_id"])
        success_level_id = int(request.POST["success_level_id"])
        remarks = request.POST.get("remarks", "")

        try:
            bpm = float(request.POST["bpm"])
        except Exception:
            bpm = -1.0

        timestamp = timestamp_or_now(request.get("timestamp", None))
    except Exception as e:
        return pyramid.httpexceptions.HTTPNotFound(str(type(e)) + ":" + str(e))

    qdb_practicesessions.add(db,
                             tune_id,
                             timestamp,
                             instrument_id,
                             success_level_id,
                             remarks,
                             bpm)
    return pyramid.response.Response()

def practice_details(db, config, username, request):
    try:
        practice_session_id = int(request.params["practice_session_id"])
    except Exception as e:
        return pyramid.httpexceptions.HTTPNotFound(str(type(e)) + ":" + str(e))

    result_data = qdb_practicesessions.details(db, practice_session_id)
    body = json.dumps(result_data)
    return pyramid.response.Response(body, content_type=json_type)

def practice_rm(db, config, username, request):
    try:
        session_ids = request.POST.getall("practice_session_id")
        session_ids = [int(si) for si in session_ids]
    except Exception as e:
        return pyramid.httpexceptions.HTTPNotFound(str(type(e)) + ":" + str(e))

    for practice_session_id in session_ids:
        qdb_practicesessions.rm(db, practice_session_id)

    return pyramid.response.Response()

def practice_suggestions(db, config, username, request):
    instrument_id = int_or_none(request.params.get("instrument_id", None))

    olds = qdb_practicesessions.suggest_olds(db, instrument_id, maxCount=5)
    news = qdb_practicesessions.suggest_news(db, instrument_id, maxCount=5)

    new_ids = set(map(lambda nt: nt["tune_id"], news))
    olds = list(filter(lambda ot: not ot["tune_id"] in new_ids,
                       olds))

    result_data = {
        "practiced" : news,
        "neglected" : olds
    }

    body = json.dumps(result_data)
    return pyramid.response.Response(body, content_type=json_type)

def tune_uri_add(db, config, username, request):
    try:
        tune_id = int(request.POST["tune_id"])
        uri_text = request.POST["uri"]
    except Exception as e:
        return pyramid.httpexceptions.HTTPNotFound(str(type(e)) + ":" + str(e))

    try:
        qdb_tunes.add_uri(db, tune_id, uri_text)
    except Exception as e:
        return pyramid.httpexceptions.HTTPNotFound(str(type(e)) + ":" + str(e))

    return pyramid.response.Response()

def tune_uri_rm(db, config, username, request):
    try:
        tune_id = int(request.POST["tune_id"])
        uri_text = request.POST["uri"]
    except Exception as e:
        return pyramid.httpexceptions.HTTPNotFound(str(type(e)) + ":" + str(e))

    qdb_tunes.rm_uri(db, tune_id, uri_text)
    return pyramid.response.Response()

def config_dict(config_path):
    result = {
        "database_dir" : ".",
        "cookie_key" : ""
        }

    if config_path == None:
        return result

    cp = configparser.ConfigParser()
    cp.read(config_path)

    try:
        result["database_dir"] = cp.get("DEFAULT", "database_dir")
    except Exception:
        pass

    try:
        result["cookie_key"] = cp.get("DEFAULT", "cookie_key")
    except Exception:
        pass

    return result

def wsgi_application(config):
    ck = config["cookie_key"]
    sf = pyramid.session.UnencryptedCookieSessionFactoryConfig(ck)

    # Pyramid configuration
    pc = pyramid.config.Configurator(session_factory=sf)

    pc.add_route("login", "/login")
    pc.add_view(view(config, login), route_name="login")

    pc.add_route("logout", "/logout")
    pc.add_view(view(config, logout), route_name="logout")

    pc.add_route("signup", "/signup")
    pc.add_view(view(config, signup),
                route_name="signup")

    pc.add_route("passwd", "/passwd")
    pc.add_view(loginny_view(config, passwd),
                route_name="passwd")

    pc.add_route("import", "/import")
    pc.add_view(view(config, import_db), route_name="import")

    pc.add_route("upgrade_db", "/upgrade_db")
    pc.add_view(loginny_view(config, upgrade_db),
                route_name="upgrade_db")

    pc.add_route("account_info", "/account_info")
    pc.add_view(loginny_view(config, account_info),
                route_name="account_info")

    pc.add_route("tune_search", "/tune_search")
    pc.add_view(loginny_view(config, tune_search),
                route_name="tune_search")

    pc.add_route("tune_details", "/tune_details")
    pc.add_view(loginny_view(config, tune_details),
                route_name="tune_details")

    pc.add_route("tune_add", "/tune_add")
    pc.add_view(loginny_view(config, tune_add),
                route_name="tune_add")

    pc.add_route("tune_rm", "/tune_rm")
    pc.add_view(loginny_view(config, tune_rm),
                route_name="tune_rm")

    pc.add_route("instruments", "/instruments")
    pc.add_view(loginny_view(config, instruments),
                route_name="instruments")

    pc.add_route("success_levels", "/success_levels")
    pc.add_view(loginny_view(config, success_levels),
                route_name="success_levels")

    pc.add_route("practice_add", "/practice_add")
    pc.add_view(loginny_view(config, practice_add),
                route_name="practice_add")

    pc.add_route("practice_details", "/practice_details")
    pc.add_view(loginny_view(config, practice_details),
                route_name="practice_details")

    pc.add_route("practice_rm", "/practice_rm")
    pc.add_view(loginny_view(config, practice_rm),
                route_name="practice_rm")

    pc.add_route("practice_suggestions", "/practice_suggestions")
    pc.add_view(loginny_view(config, practice_suggestions),
                route_name="practice_suggestions")

    pc.add_route("tune_uri_add", "/tune_uri_add")
    pc.add_view(loginny_view(config, tune_uri_add),
                route_name="tune_uri_add")

    pc.add_route("tune_uri_rm", "/tune_uri_rm")
    pc.add_view(loginny_view(config, tune_uri_rm),
                route_name="tune_uri_rm")

    return pc.make_wsgi_app()

