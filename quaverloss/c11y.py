# quaverloss/c11y.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

# Library-level compatibility tweaks, for keeping support for
# python versions from 2.7 to the latest 3.* versions.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import crypt as vanilla_crypt
import random
import sys

try:
    vanilla_crypt.crypt("")
    # Yay! crypt can generate the salt if we omit it.
    # Likely running on python 3.3+ or on a suitably patched
    # python 2.7.
    crypt = vanilla_crypt.crypt
except TypeError:
    # Ok, so we do not have a crypt.crypt function that would
    # accept just one argument. Below is a fairly naive fallback
    # that is expected to create sufficiently varying salts when
    # one is omitted. This will likely work just on GNU/Linuxes.
    #
    # One-argument crypt.crypt was added in python 3.3. Fedora
    # has backported the feature also for python 2.7, but this
    # tweak is generally necessary for pythons older than 3.3.

    chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789./"

    # Get us a random 16-sized str with characters from the
    # "chars" set above.
    def make_salt():
        return "".join((random.choice(chars) for n in range(16)))

    def crypt(word, salt=None):
        if salt == None:
            salt = "$6${}$".format(make_salt())

        return vanilla_crypt.crypt(word, salt)
