# quaverloss/dbhelp.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import os.path
import sqlite3

import quaverloss.db.maintain as dbmaint
import quaverloss.paths

class SchemaVersionMismatch(Exception):
    pass

class IdResolutionError(Exception):
  pass

# A function for producing functions of the following desctiption:
#
# Check what the database schema version is, and then call one of
# the functions that were provided in sver_map.
#
# The functions wrapped by this function can be anything, as long
# as they take the database connection as their first argument.
def schema_dependent_fn(sver_map):
    def fn(dbcon, *args, **kwargs):
        schema_ver = dbmaint.schema_version_in_db(dbcon)
        try:
            actual_fn = sver_map[schema_ver]
        except KeyError:
            err = SchemaVersionMismatch("Incompatible database layout")

            latest_sv = dbmaint.schema_version
            can_upgrade = latest_sv > schema_ver

            err.detected_schema_version = schema_ver
            err.upgrade_could_help = can_upgrade and latest_sv in sver_map

            raise err
        return actual_fn(dbcon, *args, **kwargs)

    return fn

# A function for producing functions of the following description:
#
# Find a primary key that matches a given user-visible
# name. Alternatively, if a numeric id is passed as an integer, just a
# check for its existence is made.
def pk_resolve_fn(table_name, pk_column_name, uiid_column_name):
  def resolve_id(dbcon, nameOrId):
    # For passing NULLs to sqlite
    if nameOrId == None:
      return None

    if isinstance(nameOrId, int):
      # Just check that nameOrId is a valid primary key value
      cur = dbcon.cursor()
      try:
        cur.execute("SELECT count(*) " +
                    "FROM " + table_name +
                    " WHERE " + pk_column_name + " = ?;",
                    (nameOrId,))
        if cur.fetchone()[0] != 1:
          raise IdResolutionError("No entity with id " + str(nameOrId))
      finally:
        cur.close()
      # Existence confirmed just pass the id through.
      return nameOrId

    if nameOrId == "":
      raise IdResolutionError("Empty string is not acceptable")

    cur = dbcon.cursor()
    try:
      cur.execute("SELECT " + pk_column_name + " FROM " + table_name +
                  " WHERE " + uiid_column_name + " = ?;",
                  (nameOrId,))
      rows = cur.fetchall()
      if not (len(rows) > 0):
        raise IdResolutionError("No entity with " + uiid_column_name +
                                " = " + nameOrId)
      return rows[0][0]
    finally:
      cur.close()
  return resolve_id

# Produces functions that are kind-of similar to ones from
# pk_resolve_fn. These just do not try to support any notion
# of a human-readable identifier. The resulting function will
# just ensure that an entry exists in the database, and raises
# the IdResolutionError
def existence_check_fn(table_name, pk_column_name):
    select = "SELECT count(*) FROM {} WHERE {} = ?;".format(
        table_name,
        pk_column_name
    )

    def check_existence(dbcon, thing_id):
        cur = dbcon.cursor()
        try:
            cur.execute(select, (thing_id,))
            thing_cnt = cur.fetchone()[0]
            if thing_cnt == 1:
                return

            txt = "No entity with id {}".format(thing_id)
            raise IdResolutionError(txt)
        finally:
            cur.close()
    return check_existence

def noprint(arg):
  pass

# Open the database.
#
# A plain sqlite3.connect() invocation will not do all the
# things that are desirable to have in all cases where the
# Quaverloss database is opened. This function does the
# open + adds the missing bits.
def open_db(path=None, print_info=noprint):
    if path == None:
        path = quaverloss.paths.dbfile_path()
        ddir = os.path.dirname(path)
        quaverloss.paths.ensure_dir(ddir)
        print_info("automatically chose \"" + path + "\" as db path")

    didExist = os.path.exists(path)
    dbcon = sqlite3.connect(path)
    if not didExist:
        print_info("Database did not exist before; resetting")
        dbmaint.reset_db(dbcon)
    else:
        print_info("Looks like database was already there")

    cur = dbcon.cursor()
    try:
        cur.execute("PRAGMA foreign_keys = ON;", ())
    finally:
        cur.close()
    return dbcon

def memdb(schema_version=dbmaint.schema_version):
  dbcon = sqlite3.connect(":memory:")
  dbmaint.reset_db(dbcon, schema_ver=schema_version)
  cur = dbcon.cursor()
  try:
    cur.execute("PRAGMA foreign_keys = ON;", ())
  finally:
    cur.close()
  return dbcon
