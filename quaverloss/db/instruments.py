# quaverloss/db/instruments.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import quaverloss.dbhelp as dbhelp

resolve_id_v1 = dbhelp.pk_resolve_fn("instrument", "instrumentId", "name")
resolve_id_v2 = dbhelp.pk_resolve_fn("instrument", "instrument_id", "name")

resolve_id = dbhelp.schema_dependent_fn({
        1: resolve_id_v1,
        2: resolve_id_v2,
    })

def add_v1(dbcon, name):
  if len(name) == 0:
    raise Exception("Instument with an empty name is not allowed")
  cur = dbcon.cursor()
  try:
    cur.execute("INSERT INTO instrument(name) VALUES(?);", (name,))
  finally:
    cur.close()

add = dbhelp.schema_dependent_fn({
        1: add_v1,
        2: add_v1,
    })

def rm_v1(dbcon, nameOrId):
  iid = resolve_id(dbcon, nameOrId)
  cur = dbcon.cursor()
  try:
    cur.execute("DELETE FROM instrument WHERE instrumentId = ?;",
                (iid,))
  finally:
    cur.close()

def rm_v2(dbcon, name_or_id):
    iid = resolve_id(dbcon, name_or_id)
    cur = dbcon.cursor()
    try:
        cur.execute("DELETE FROM instrument WHERE instrument_id = ?;",
                    (iid,))
    finally:
        cur.close()

rm = dbhelp.schema_dependent_fn({
        1: rm_v1,
        2: rm_v2,
    })

def set_fields_v1(dbcon, nameOrId, attrDict):
  newName = attrDict.get("name", None)

  if newName == "":
    raise Exception("Instument with an empty name is not allowed")

  iid = resolve_id(dbcon, nameOrId)

  cur = dbcon.cursor()
  try:
    if newName != None:
      cur.execute("UPDATE instrument SET name = ? WHERE instrumentId = ?;",
                  (newName, iid))
  finally:
    cur.close()

def set_fields_v2(dbcon, name_or_id, attr_dict):
    new_name = attr_dict.get("name", None)
    if new_name == "":
        raise Exception("Instument with an empty name is not allowed")

    iid = resolve_id(dbcon, name_or_id)
    cur = dbcon.cursor()
    try:
        if new_name != None:
            cmd = """
UPDATE instrument SET name = ? WHERE instrument_id = ?;
"""
            cur.execute(cmd, (new_name, iid))
    finally:
        cur.close()

set_fields = dbhelp.schema_dependent_fn({
        1: set_fields_v1,
        2: set_fields_v2,
    })

def by_name_v1(dbcon, nameOrId):
  def row_to_dict(row):
    return { "name" : row[0] }

  iid = resolve_id(dbcon, nameOrId)

  cur = dbcon.cursor()
  try:
    cur.execute("SELECT name FROM instrument WHERE instrumentId = ?;",
                (iid,))
    return row_to_dict(cur.fetchone())
  finally:
    cur.close()

def by_name_v2(dbcon, name_or_id):
    def row_to_dict(row):
        return {
            "name" : row[0],
        }

    iid = resolve_id(dbcon, name_or_id)
    cur = dbcon.cursor()
    try:
        cur.execute("SELECT name FROM instrument WHERE instrument_id = ?;",
                    (iid,))
        return row_to_dict(cur.fetchone())
    finally:
        cur.close()

by_name = dbhelp.schema_dependent_fn({
        1: by_name_v1,
        2: by_name_v2,
    })

def _get_all_row_to_dict(row):
    d = {
        "instrument_id": row[0],
        "instrument_name": row[1]
    }
    return d

def get_all_v1(dbcon):
    cur = dbcon.cursor()
    try:
        select_stmt = """
SELECT
    instrumentId,
    name
FROM
    instrument
ORDER BY name;
"""
        cur.execute(select_stmt)
        return [_get_all_row_to_dict(row) for row in cur.fetchall()]
    finally:
        cur.close()

def get_all_v2(dbcon):
    cur = dbcon.cursor()
    try:
        select_stmt = """
SELECT
    instrument_id,
    name
FROM
    instrument
ORDER BY name;
"""
        cur.execute(select_stmt)
        return [_get_all_row_to_dict(row) for row in cur.fetchall()]
    finally:
        cur.close()

get_all = dbhelp.schema_dependent_fn({
        1: get_all_v1,
        2: get_all_v2,
    })
