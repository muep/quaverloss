-- Copyright 2013 Joonas Sarajärvi
--
-- This file is part of Quaverloss.
--
-- Quaverloss is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- Quaverloss is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public
-- License along with Quaverloss. In source copies of Quaverloss, it is
-- usually included in files named COPYING and COPYING.LESSER. The
-- license is also available at <http://www.gnu.org/licenses/>.
--
-- db_v1_v2.sql
--
-- Statements for upgrading a Quaverloss database from
-- schema version 1 to schema version 2.

-- Due to a naming convention change, can not just modify
-- tables in place. The script first just renames the old
-- ones and then creates new tables into their place.
ALTER TABLE tune RENAME TO tune_v1;
ALTER TABLE successLevel RENAME TO successLevel_v1;
ALTER TABLE instrument RENAME TO instrument_v1;
ALTER TABLE practiceSession RENAME TO practiceSession_v1;
ALTER TABLE config RENAME TO config_v1;

CREATE TABLE tune (
    tune_id INTEGER PRIMARY KEY UNIQUE,
    name TEXT UNIQUE NOT NULL
);

INSERT
    INTO tune(tune_id, name)
    SELECT tuneId, name FROM tune_v1;

CREATE TABLE tune_uri (
    tune_uri_id INTEGER PRIMARY KEY UNIQUE,
    tune_id INTEGER NOT NULL REFERENCES tune(tune_id) ON DELETE CASCADE,
    uri TEXT NOT NULL DEFAULT ''
);

INSERT
    INTO tune_uri (
        tune_id,
        uri
    )
    SELECT
        tuneId,
        thesessionUri
    FROM tune_v1
    WHERE length(thesessionUri) > 0;

CREATE TABLE success_level (
    success_level_id INTEGER PRIMARY KEY UNIQUE,
    name TEXT UNIQUE NOT NULL,
    niceness INTEGER UNIQUE NOT NULL,
    description TEXT NOT NULL DEFAULT ''
);

INSERT
    INTO success_level (
        success_level_id,
        name,
        niceness,
        description
    )
    SELECT
        successLevelId,
        name,
        niceness,
        description
    FROM successLevel_v1;

CREATE TABLE instrument (
    instrument_id INTEGER PRIMARY KEY UNIQUE,
    name TEXT UNIQUE NOT NULL
);

INSERT
    INTO instrument(
        instrument_id,
        name
    )
    SELECT
        instrumentId,
        name
    FROM instrument_v1;

CREATE TABLE practice_session (
    practice_session_id INTEGER PRIMARY KEY UNIQUE,
    tune_id INTEGER NOT NULL
        REFERENCES tune(tune_id) ON DELETE RESTRICT,
    instrument_id INTEGER NOT NULL
        REFERENCES instrument(instrument_id) ON DELETE RESTRICT,
    success_level_id INTEGER NOT NULL
        REFERENCES success_level(success_level_id) ON DELETE RESTRICT,
    practice_stamp INTEGER NOT NULL,
    remarks TEXT NOT NULL DEFAULT ''
);

INSERT
    INTO practice_session (
        practice_session_id,
        tune_id,
        instrument_id,
        success_level_id,
        practice_stamp,
        remarks
    )
    SELECT
        practiceSessionId,
        tuneId,
        instrumentId,
        successLevelId,
        practiceStamp,
        remarks
    FROM practiceSession_v1;

CREATE TABLE practice_session_bpm (
    practice_session_id INTEGER UNIQUE NOT NULL
        REFERENCES practice_session(practice_session_id)
        ON DELETE CASCADE,
    bpm REAL NOT NULL
);

INSERT
    INTO practice_session_bpm (
        practice_session_id,
        bpm
    )
    SELECT
        practiceSessionId,
        bpm
    FROM practiceSession_v1
    WHERE bpm > 0;

-- Just a rename of the config_id column.
CREATE TABLE config (
  config_id INTEGER PRIMARY KEY UNIQUE,
  key TEXT UNIQUE NOT NULL,
  value TEXT NOT NULL
);

INSERT
    INTO config (
        config_id,
        key,
        value
    )
    SELECT
        configId,
        key,
        value
    FROM config_v1;

UPDATE config SET value = '2' WHERE key = 'schema';

DROP TABLE config_v1;
DROP TABLE practiceSession_v1;
DROP TABLE successLevel_v1;
DROP TABLE instrument_v1;
DROP TABLE tune_v1;
