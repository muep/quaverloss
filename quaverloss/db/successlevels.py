# quaverloss/db/successlevels.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import quaverloss.dbhelp as dbhelp

resolve_id_v1 = dbhelp.pk_resolve_fn("successLevel",
                                     "successLevelId",
                                     "name")
resolve_id_v2 = dbhelp.pk_resolve_fn("success_level",
                                     "success_level_id",
                                     "name")

resolve_id = dbhelp.schema_dependent_fn({
        1: resolve_id_v1,
        2: resolve_id_v2,
    })

def add_v1(dbcon, name, niceness, description):
  if len(name) == 0:
    raise Exception("Success level with an empty name is not allowed")
  cur = dbcon.cursor()
  try:
    cur.execute(
      "INSERT INTO successLevel(name, niceness, description)" +
      " VALUES(?,?,?);",
      (name, niceness, description)
    )
  finally:
    cur.close()

def add_v2(dbcon, name, niceness, description):
    if len(name) == 0:
        raise Exception("Success level with an empty name is not allowed")
    cur = dbcon.cursor()
    try:
        insert = """
INSERT INTO success_level(name, niceness, description) VALUES(?,?,?);
"""
        cur.execute(insert, (name, niceness, description))
    finally:
        cur.close()

add = dbhelp.schema_dependent_fn({
        1: add_v1,
        2: add_v2,
    })

def rm_v1(dbcon, nameOrId):
  lvlId = resolve_id(dbcon, nameOrId)

  cur = dbcon.cursor()
  try:
    cur.execute("DELETE FROM successLevel WHERE successLevelId = ?;",
                (lvlId,))
  finally:
    cur.close()

def rm_v2(dbcon, name_or_id):
    lvl_id = resolve_id(dbcon, name_or_id)
    cur = dbcon.cursor()
    try:
        delete = """
DELETE FROM success_level WHERE success_level_id = ?;
"""
        cur.execute(delete, (lvl_id,))
    finally:
        cur.close()

rm = dbhelp.schema_dependent_fn({
        1: rm_v1,
        2: rm_v2,
    })

def _row_to_dict(row):
    d = {
        "success_level_id": row[0],
        "name": row[1],
        "niceness": row[2],
        "description": row[3],
    }
    return d

def by_name_v1(dbcon, nameOrId):
    lvl_id = resolve_id(dbcon, nameOrId)

    cur = dbcon.cursor()
    try:
        select_stmt = """
SELECT
    successLevelId,
    name,
    niceness,
    description
FROM
    successLevel
WHERE
    successLevelId = ?;
"""
        cur.execute(select_stmt,(lvl_id,))
        return _row_to_dict(cur.fetchone())
    finally:
        cur.close()

def by_name_v2(dbcon, name_or_id):
    lvl_id = resolve_id(dbcon, name_or_id)

    cur = dbcon.cursor()
    try:
        select = """
SELECT
    success_level_id,
    name,
    niceness,
    description
FROM success_level
WHERE success_level_id = ?;
"""
        cur.execute(select, (lvl_id,))
        return _row_to_dict(cur.fetchone())
    finally:
        cur.close()

by_name = dbhelp.schema_dependent_fn({
        1: by_name_v1,
        2: by_name_v2,
    })

# A blunt tool like this should be just fine for the small number
# of levels expected.
def get_all_v1(dbcon):
    cur = dbcon.cursor()
    try:
        select_stmt = """
SELECT
    successLevelId,
    name,
    niceness,
    description
FROM
    successLevel
ORDER BY niceness;
"""
        cur.execute(select_stmt)
        return [_row_to_dict(row) for row in cur.fetchall()]
    finally:
        cur.close()

def get_all_v2(dbcon):
    cur = dbcon.cursor()
    try:
        select = """
SELECT
    success_level_id,
    name,
    niceness,
    description
FROM success_level
ORDER BY niceness;
"""
        cur.execute(select)
        return [_row_to_dict(row) for row in cur.fetchall()]
    finally:
        cur.close()

get_all = dbhelp.schema_dependent_fn({
        1: get_all_v1,
        2: get_all_v2,
    })

def set_fields_v1(dbcon, nameOrId, attrDict):
  newName = attrDict.get("name", None)
  newNiceness = attrDict.get("niceness", None)
  newDesc = attrDict.get("description", None)

  if newName == "":
    raise Exception("Empty name is not allowed for success levels")

  slId = resolve_id(dbcon, nameOrId)
  cur = dbcon.cursor()

  try:
    if newName != None:
      cur.execute(
        "UPDATE successLevel SET name = ? WHERE successLevelId = ?;",
        (newName, slId)
        )
    if newNiceness != None:
      cur.execute(
        "UPDATE successLevel SET niceness = ? WHERE successLevelId = ?;",
        (newNiceness, slId)
        )
    if newDesc != None:
      cur.execute(
        "UPDATE successLevel SET description = ? WHERE successLevelId = ?;",
        (newDesc, slId)
        )

  finally:
    cur.close()

def set_fields_v2(dbcon, name_or_id, attr_dict):
    new_name = attr_dict.get("name", None)
    new_niceness = attr_dict.get("niceness", None)
    new_desc = attr_dict.get("description", None)

    if new_name == "":
        raise Exception("Empty name is not allowed for success levels")

    lvl_id = resolve_id(dbcon, name_or_id)
    cur = dbcon.cursor()
    try:
        if new_name != None:
            update_name = """
UPDATE success_level SET name = ? WHERE success_level_id = ?;
"""
            cur.execute(update_name, (new_name, lvl_id))
        if new_niceness != None:
            update_niceness = """
UPDATE success_level SET niceness = ? WHERE success_level_id = ?;
"""
            cur.execute(update_niceness, (new_niceness, lvl_id))
        if new_desc != None:
            update_desc = """
UPDATE success_level SET description = ? WHERE success_level_id = ?;
"""
            cur.execute(update_desc, (new_desc, lvl_id))
    finally:
        cur.close()

set_fields = dbhelp.schema_dependent_fn({
        1: set_fields_v1,
        2: set_fields_v2,
    })
