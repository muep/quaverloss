-- Copyright 2012 Joonas Sarajärvi
--
-- This file is part of Quaverloss.
--
-- Quaverloss is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- Quaverloss is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public
-- License along with Quaverloss. In source copies of Quaverloss, it is
-- usually included in files named COPYING and COPYING.LESSER. The
-- license is also available at <http://www.gnu.org/licenses/>.

-- SQLite database creation statements for the tune logger.
-- Still very much work in progress and subject to change, especially
-- as long the goal posts keep moving.

DROP TABLE IF EXISTS tune;
DROP TABLE IF EXISTS successLevel;
DROP TABLE IF EXISTS instrument;
DROP TABLE IF EXISTS practiceSession;
DROP TABLE IF EXISTS config;

-- Tunes mostly consist of just their names. However, since there can
-- pretty much exist one addStamp and one thession.org URI per tune,
-- they are also directly in the tune table.
CREATE TABLE tune (
  tuneId INTEGER PRIMARY KEY UNIQUE,
  name TEXT UNIQUE NOT NULL,
  thesessionUri TEXT NOT NULL DEFAULT ''
);

-- Not yet clear how a machine-readable evaluation of progress should
-- be recorded. Something like this might work;
CREATE TABLE successLevel (
  successLevelId INTEGER PRIMARY KEY UNIQUE,
  -- User is intended to use this when referring to levels
  name TEXT UNIQUE NOT NULL,
  -- This is used for recognizing which levels are "good"
  -- and which are less good. A higher number will signify
  -- a more desirable outcome. The absolute value of this is not
  -- intended to be used for anything.
  niceness INTEGER UNIQUE NOT NULL,
  description TEXT NOT NULL DEFAULT ''
);

CREATE TABLE instrument (
  instrumentId INTEGER PRIMARY KEY UNIQUE,
  name TEXT UNIQUE NOT NULL
);

-- Progress in practicing is mainly intended to be recorder here
CREATE TABLE practiceSession (
  practiceSessionId INTEGER PRIMARY KEY UNIQUE,
  -- Here we actually restrict deletions to avoid accidentally
  -- losing practice history.
  tuneId INTEGER NOT NULL REFERENCES tune(tuneId) ON DELETE RESTRICT,
  instrumentId INTEGER NOT NULL
    REFERENCES instrument(instrumentId) ON DELETE RESTRICT,
  practiceStamp INTEGER NOT NULL,
  successLevelId INTEGER NOT NULL
    REFERENCES successLevel(successLevelId) ON DELETE RESTRICT,
  remarks TEXT NOT NULL DEFAULT '',
  bpm REAL NOT NULL DEFAULT -1
);

-- These might be better stored in a configuration file.
CREATE TABLE config (
  configId INTEGER PRIMARY KEY UNIQUE,
  key TEXT UNIQUE NOT NULL,
  value TEXT NOT NULL
);
