# quaverloss/db/practicesessions.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import datetime

import quaverloss.dbhelp           as dbhelp
import quaverloss.db.instruments   as instruments
import quaverloss.db.successlevels as successlevels
import quaverloss.db.tunes         as tunes

require_existence_v1 = dbhelp.existence_check_fn("practiceSession",
                                                 "practiceSessionId")
require_existence_v2 = dbhelp.existence_check_fn("practice_session",
                                                 "practice_session_id")

require_existence = dbhelp.schema_dependent_fn({
        1: require_existence_v1,
        2: require_existence_v2,
    })

def add_v1(dbcon,
        tuneIdOrName,
        practiceStamp,
        instrumentIdOrName,
        successLevelIdOrName,
        remarks,
        bpm):
  tuneId = tunes.resolve_id(dbcon, tuneIdOrName)
  instrumentId = instruments.resolve_id(dbcon, instrumentIdOrName)
  levelId = successlevels.resolve_id(dbcon, successLevelIdOrName)

  stamp = int(practiceStamp)

  cur = dbcon.cursor()
  try:
    cur.execute(
      "INSERT INTO practiceSession (" +
      "  tuneId, instrumentId, practiceStamp," +
      "  successLevelId, remarks, bpm)" +
      "VALUES(?,?,?,?,?,?);",
      (tuneId, instrumentId, stamp, levelId, remarks, bpm))
  finally:
    cur.close()

def add_v2(dbcon,
           tune_id_or_name,
           practice_stamp,
           instrument_id_or_name,
           success_level_id_or_name,
           remarks,
           bpm):
    tune_id = tunes.resolve_id(dbcon, tune_id_or_name)
    instrument_id = instruments.resolve_id(dbcon, instrument_id_or_name)
    slvl_id = successlevels.resolve_id(dbcon, success_level_id_or_name)

    stamp = int(practice_stamp)

    cur = dbcon.cursor()
    try:
        session_insert_stmt = """
INSERT INTO practice_session (
    tune_id,
    instrument_id,
    success_level_id,
    practice_stamp,
    remarks
)
VALUES (?, ?, ?, ?, ?);
"""
        params = (
            tune_id,
            instrument_id,
            slvl_id,
            practice_stamp,
            remarks
        )
        cur.execute(session_insert_stmt, params)
        practice_session_id = cur.lastrowid

        if bpm > 0:
            bpm_insert_stmt = """
INSERT INTO practice_session_bpm (
    practice_session_id,
    bpm
)
VALUES(?, ?);
"""
            bpm_insert_params = (practice_session_id, bpm)
            cur.execute(bpm_insert_stmt, bpm_insert_params)
    finally:
        pass

add = dbhelp.schema_dependent_fn({
        1: add_v1,
        2: add_v2,
    })

def rm_v1(dbcon, practice_session_id):
    require_existence(dbcon, practice_session_id)

    cur = dbcon.cursor()
    try:
        delete_stmt = """
DELETE FROM practiceSession WHERE practiceSessionId = ?;
"""
        cur.execute(delete_stmt, (practice_session_id,))
    finally:
        cur.close()

def rm_v2(dbcon, practice_session_id):
    require_existence(dbcon, practice_session_id)

    cur = dbcon.cursor()
    try:
        delete_stmt = """
DELETE FROM practice_session WHERE practice_session_id = ?;
"""
        cur.execute(delete_stmt, (practice_session_id,))
    finally:
        cur.close()

rm = dbhelp.schema_dependent_fn({
        1: rm_v1,
        2: rm_v2,
    })

def _summary_row_to_dict(row):
    d = {
        "practice_session_id": row[0],
        "tune_name": row[1],
        "instrument_name": row[2],
        "success_level_name": row[3],
        "practice_session_bpm": row[4],
        "practice_session_practice_stamp": row[5]
    }
    return d


def summary_all_v1(dbcon):

  cur = dbcon.cursor()
  try:
    cur.execute(
      "SELECT" +
      " practiceSession.practiceSessionId," +
      " tune.name," +
      " instrument.name," +
      " successLevel.name," +
      " practiceSession.bpm," +
      " practiceSession.practiceStamp " +
      "FROM practiceSession" +
      " JOIN tune ON" +
      "  practiceSession.tuneId = tune.tuneId" +
      " JOIN instrument ON" +
      "  practiceSession.instrumentId = instrument.instrumentId" +
      " JOIN successLevel ON" +
      "  practiceSession.successLevelId = successLevel.successLevelId " +
      "ORDER BY practiceSession.practiceStamp;",
      ())

    return list(map(_summary_row_to_dict, cur.fetchall()))
  finally:
    cur.close()

def summary_all_v2(dbcon):
    cur = dbcon.cursor()
    try:
        select_stmt = """
SELECT
    practice_session.practice_session_id,
    tune.name,
    instrument.name,
    success_level.name,
    practice_session_bpm.bpm,
    practice_session.practice_stamp
FROM
    practice_session
    JOIN tune ON
        tune.tune_id = practice_session.tune_id
    JOIN instrument ON
        instrument.instrument_id = practice_session.instrument_id
    JOIN success_level ON
        practice_session.success_level_id = success_level.success_level_id
    LEFT OUTER JOIN practice_session_bpm ON
        practice_session_bpm.practice_session_id =
            practice_session.practice_session_id
ORDER BY
    practice_session.practice_stamp;
"""
        cur.execute(select_stmt)
        return [_summary_row_to_dict(row) for row in cur.fetchall()]
    finally:
        cur.close()

summary_all = dbhelp.schema_dependent_fn({
        1: summary_all_v1,
        2: summary_all_v2,
    })

def _unpracticeds2_row_to_dict(row):
    d = {
        "tune_id": row[0],
        "tune_name": row[1],
        # Some keys for providing limited compatibility with the
        # "suggest" function.
        "instrument_id": None,
        "instrument_name": None,
        "last_success_level_id": None,
        "last_success_level_name": None,
        "last_practice_stamp": None
    }
    return d

def unpracticeds2_v1(dbcon, instrument):
  instrumentId = instruments.resolve_id(dbcon, instrument)
  cur = dbcon.cursor()
  try:
    cur.execute(
"""
  SELECT tune.tuneId, tune.name FROM tune
    EXCEPT
  SELECT tune.tuneId, tune.name FROM
    tune JOIN practiceSession
      ON tune.tuneId = practiceSession.tuneId
    WHERE (? IS NULL) OR (practiceSession.instrumentId = ?)
  ORDER BY tune.name;
           """, (instrumentId, instrumentId))
    return list(map(_unpracticeds2_row_to_dict, cur.fetchall()))
  finally:
    cur.close()

def unpracticeds2_v2(dbcon, instrument):
    instrument_id = instruments.resolve_id(dbcon, instrument)
    cur = dbcon.cursor()
    try:
        select_stmt = """
SELECT
    tune.tune_id,
    tune.name
FROM tune
    EXCEPT
SELECT
    tune.tune_id,
    tune.name
FROM
    tune
    JOIN practice_session ON
        tune.tune_id = practice_session.tune_id
WHERE
    (? IS NULL) OR (practice_session.instrument_id = ?)
ORDER BY tune.name;
"""
        params = (instrument_id, instrument_id)

        cur.execute(select_stmt, params)

        return [_unpracticeds2_row_to_dict(row) for row in cur.fetchall()]
    finally:
        cur.close()

unpracticeds2 = dbhelp.schema_dependent_fn({
        1: unpracticeds2_v1,
        2: unpracticeds2_v2,
    })

def _suggestion_row_to_dict(row):
  return { "tune_id" : row[0],
           "tune_name" : row[1],
           "instrument_id" : row[2],
           "instrument_name" : row[3],
           "last_success_level_id" : row[4],
           "last_success_level_name" : row[5],
           "last_practice_stamp" : row[6] }


def suggest_backend_v1(dbcon,
                   instrument,
                   olds_wanted = False,
                   maxCount = 5):
  instrumentId = instruments.resolve_id(dbcon, instrument)

  order_dir = "ASC" if olds_wanted else "DESC";

  cur0 = dbcon.cursor()
  cur1 = dbcon.cursor()
  try:
    summary_rows = []

    # This task used to be done with a huge single query, but it looks
    # like that solution relied on undefined behavior of sqlite3.
    #
    # The current implementation tries to better consider that mixing
    # aggregate queries and non-aggregate ones can produce a bit
    # surprising results.

    # So first, fetch just the tunes themselves.
    cur0.execute(
      "SELECT" +
      " tune.tuneId," +
      " tune.name," +
      " max(practiceSession.practiceStamp) AS lastPractice " +
      "FROM tune JOIN practiceSession" +
      " ON practiceSession.tuneId = tune.tuneId " +
      "WHERE ((? IS NULL) OR (? = practiceSession.instrumentId)) " +
      "GROUP BY tune.tuneId " +
      "ORDER BY lastPractice " + order_dir + " " +
      "LIMIT ?;",
      (instrumentId, instrumentId, maxCount)
      )

    tune_row = cur0.fetchone()
    while tune_row != None:
      # Fetch additional details for each tune that was selected.
      # It is a bit unfortunate that another heavy selection is
      # necessary for the practiceSession table.

      tuneId = tune_row[0]
      cur1.execute(
        "SELECT" +
        " instrument.instrumentId," +
        " instrument.name," +
        " successLevel.successLevelId," +
        " successLevel.name," +
        " practiceSession.practiceStamp " +
        "FROM practiceSession " +
        " JOIN successLevel ON" +
        "  practiceSession.successLevelId " +
        "   = successLevel.successLevelId" +
        " JOIN instrument ON" +
        "        instrument.instrumentId" +
        " = practiceSession.instrumentId " +
        "WHERE " +
        " (practiceSession.tuneId = ?) AND" +
        "  ((? IS NULL) OR (? = practiceSession.instrumentId)) " +
        "ORDER BY practiceSession.practiceStamp DESC " +
        "LIMIT 1;",
        (tuneId, instrumentId, instrumentId))

      detail_row = cur1.fetchone()
      if detail_row != None:
        tune_and_details = tune_row[:2] + detail_row
        summary_rows.append(_suggestion_row_to_dict(tune_and_details))

      tune_row = cur0.fetchone()

    if olds_wanted:
      summary_rows.reverse()

    return summary_rows
  finally:
    cur0.close()
    cur1.close()

def suggest_backend_v2(dbcon,
                       instrument,
                       olds_wanted = False,
                       maxCount = 5):
    instrument_id = instruments.resolve_id(dbcon, instrument)

    order_dir = "ASC" if olds_wanted else "DESC"

    cur0 = dbcon.cursor()
    cur1 = dbcon.cursor()
    try:
        results = []

        tune_select = """
SELECT
    tune.tune_id,
    tune.name,
    max(practice_session.practice_stamp) AS last_practice
FROM
    tune
    JOIN practice_session ON
        practice_session.tune_id = tune.tune_id
WHERE
    ((? IS NULL) OR (? = practice_session.instrument_id))
GROUP BY
    tune.tune_id
ORDER BY
    last_practice {order_dir}
LIMIT ?;
""".format(order_dir=order_dir)
        tune_select_params = (
            instrument_id, instrument_id,
            maxCount
        )
        cur0.execute(tune_select, tune_select_params)
        for tune_row in cur0:
            tune_id = tune_row[0]

            detail_select = """
SELECT
    instrument.instrument_id,
    instrument.name,
    success_level.success_level_id,
    success_level.name,
    practice_session.practice_stamp
FROM
    practice_session
    JOIN instrument ON
        practice_session.instrument_id = instrument.instrument_id
    JOIN success_level ON
        practice_session.success_level_id = success_level.success_level_id
WHERE
    (practice_session.tune_id = ?) AND
    ((? IS NULL) OR (? = practice_session.instrument_id))
ORDER BY practice_session.practice_stamp DESC
LIMIT 1;
"""
            detail_select_params = (
                tune_id,
                instrument_id, instrument_id
            )
            cur1.execute(detail_select, detail_select_params)
            detail_row = cur1.fetchone()

            if detail_row != None:
                tune_and_details = tune_row[:2] + detail_row
                results.append(_suggestion_row_to_dict(tune_and_details))

        if olds_wanted:
            results.reverse()

        return results
    finally:
        cur0.close()
        cur1.close()

suggest_backend = dbhelp.schema_dependent_fn({
        1: suggest_backend_v1,
        2: suggest_backend_v2,
    })

# Another take at fetching a set tunes to play
#
# * Gives a fixed maximum number of tunes.
#
# * Prefers tunes whose last practice stamp is the lowest.
#
# * Ignores tunes which have never been practiced
def suggest_olds(dbcon,
                 instrument,
                 maxCount = 5):
  return suggest_backend(dbcon, instrument,
                         olds_wanted=True,
                         maxCount = maxCount)

# Clone of suggest_olds, but so that it prefers recently practiced
# tunes.
def suggest_news(dbcon,
                 instrument,
                 maxCount = 5):
  return suggest_backend(dbcon, instrument,
                         olds_wanted=False,
                         maxCount = maxCount)

def _recents_row_to_dict(row):
    d = {
        "tune_id": row[0],
        "tune_name": row[1],
        "instrument_id": row[2],
        "instrument_name": row[3],
        "success_level_id": row[4],
        "success_level_name": row[5],
        "practice_bpm": row[6],
        "practice_stamp": row[7],
        "practice_session_id": row[8],
    }
    return d

def recents_v1(dbcon,
            tune = None,
            instrument = None,
            maxCount = 5):
    tuneId = tunes.resolve_id(dbcon, tune)
    instrumentId = instruments.resolve_id(dbcon, instrument)

    cur = dbcon.cursor()
    try:
        select_stmt = """
  SELECT tune.tuneId,
         tune.name,
         instrument.instrumentId,
         instrument.name,
         successLevel.successLevelId,
         successLevel.name,
         practiceSession.bpm,
         practiceSession.practiceStamp,
         practiceSession.practiceSessionId
  FROM tune
    JOIN practiceSession
     ON practiceSession.tuneId = tune.tuneId
    JOIN instrument
     ON practiceSession.instrumentId = instrument.instrumentId
    JOIN successLevel
     ON practiceSession.successLevelId = successLevel.successLevelId
  WHERE ((? IS NULL) OR (? = instrument.instrumentId)) AND
        ((? IS NULL) OR (? = tune.tuneId))
  ORDER BY practiceSession.practiceStamp DESC
  """
        if maxCount != None:
            select_stmt += " LIMIT ?;"
        else:
            select_stmt += ";"

        params = (
            instrumentId, instrumentId,
            tuneId, tuneId
            )

        if maxCount != None:
            params += (maxCount,)

        cur.execute(select_stmt, params)
        return list(map(_recents_row_to_dict, cur.fetchall()))
    finally:
        cur.close()

def recents_v2(dbcon,
               tune = None,
               instrument = None,
               maxCount = 5):
    tune_id = tunes.resolve_id(dbcon, tune)
    instrument_id = instruments.resolve_id(dbcon, instrument)

    cur = dbcon.cursor()
    try:
        select_stmt = """
SELECT
    tune.tune_id,
    tune.name,
    instrument.instrument_id,
    instrument.name,
    success_level.success_level_id,
    success_level.name,
    practice_session_bpm.bpm,
    practice_session.practice_stamp,
    practice_session.practice_session_id
FROM
    tune
    JOIN practice_session ON
        practice_session.tune_id = tune.tune_id
    JOIN instrument ON
        practice_session.instrument_id = instrument.instrument_id
    JOIN success_level ON
        practice_session.success_level_id = success_level.success_level_id
    LEFT OUTER JOIN practice_session_bpm ON
        practice_session_bpm.practice_session_id =
        practice_session.practice_session_id
WHERE
    ((? IS NULL) OR (? = instrument.instrument_id)) AND
    ((? IS NULL) OR (? = tune.tune_id))
ORDER BY practice_session.practice_stamp DESC
"""
        if maxCount != None:
            select_stmt += " LIMIT ?;"
        else:
            select_stmt += ";"
        params = (
            instrument_id, instrument_id,
            tune_id, tune_id,
        )
        if maxCount != None:
            params += (maxCount,)

        cur.execute(select_stmt, params)
        return [_recents_row_to_dict(row) for row in cur.fetchall()]
    finally:
        cur.close()

# Search for practice sessions.
#
# recents(dbcon,
#         tune=None,
#         instrument=None,
#         maxCount=5)
recents = dbhelp.schema_dependent_fn({
        1: recents_v1,
        2: recents_v2,
    })

def _details_row_to_dict(row):
    d = {
        "tune_id": row[0],
        "tune_name": row[1],
        "instrument_id": row[2],
        "instrument_name": row[3],
        "success_level_id": row[4],
        "success_level_name": row[5],
        "practice_bpm": row[6],
        "practice_stamp": row[7],
        "practice_session_id": row[8],
        "practice_remarks" : row[9],
    }
    return d

def details_v1(dbcon, practice_session_id):
    require_existence(dbcon, practice_session_id)

    cur = dbcon.cursor()
    try:
        select_stmt = """
SELECT
    tune.tuneId,
    tune.name,
    instrument.instrumentId,
    instrument.name,
    successLevel.successLevelId,
    successLevel.name,
    practiceSession.bpm,
    practiceSession.practiceStamp,
    practiceSession.practiceSessionId,
    practiceSession.remarks
FROM tune
    JOIN practiceSession ON
        practiceSession.tuneId = tune.tuneId
    JOIN instrument ON
        practiceSession.instrumentId = instrument.instrumentId
    JOIN successLevel ON
        practiceSession.successLevelId = successLevel.successLevelId
WHERE practiceSession.practiceSessionId = ?;
"""
        cur.execute(select_stmt, (practice_session_id,))
        return _details_row_to_dict(cur.fetchone())
    finally:
        cur.close()

def details_v2(dbcon, practice_session_id):
    require_existence(dbcon, practice_session_id)

    cur = dbcon.cursor()
    try:
        select_stmt = """
SELECT
    tune.tune_id,
    tune.name,
    instrument.instrument_id,
    instrument.name,
    success_level.success_level_id,
    success_level.name,
    practice_session_bpm.bpm,
    practice_session.practice_stamp,
    practice_session.practice_session_id,
    practice_session.remarks
FROM
    tune
    JOIN practice_session ON
        practice_session.tune_id = tune.tune_id
    JOIN instrument ON
        practice_session.instrument_id = instrument.instrument_id
    JOIN success_level ON
        practice_session.success_level_id = success_level.success_level_id
    LEFT OUTER JOIN practice_session_bpm ON
        practice_session_bpm.practice_session_id =
        practice_session.practice_session_id
WHERE practice_session.practice_session_id = ?;
"""
        cur.execute(select_stmt, (practice_session_id,))
        return _details_row_to_dict(cur.fetchone())
    finally:
        cur.close()

details = dbhelp.schema_dependent_fn({
        1: details_v1,
        2: details_v2,
    })
