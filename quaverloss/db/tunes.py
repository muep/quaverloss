# quaverloss/db/tunes.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import sqlite3

import quaverloss.dbhelp as dbhelp

resolve_id_v1 = dbhelp.pk_resolve_fn("tune", "tuneId", "name")
resolve_id_v2 = dbhelp.pk_resolve_fn("tune", "tune_id", "name")

resolve_id = dbhelp.schema_dependent_fn({
        1: resolve_id_v1,
        2: resolve_id_v2,
    })

# Add a tune
def add_v1(dbcon, name, uris=[]):
  if len(name) == 0:
    raise Exception("Will not add a tune with an empty name")

  if len(uris) == 0:
    thesessionUri = ""
  elif len(uris) == 1:
    thesessionUri = uris[0]
  else:
    raise Exception("Database layout 1 can not hold multiple tune URIs")

  cur = dbcon.cursor()
  try:
    cur.execute("""
      INSERT INTO tune(name, thesessionUri)
      VALUES(?,?);""",
      (name, thesessionUri)
    )
    pass
  except sqlite3.IntegrityError as e:
    if str(e) == "column name is not unique":
      raise Exception("tune \"" + name + "\" is already in db")
    else:
      raise e
  finally:
    cur.close()

# Add a tune
def add_v2(dbcon, name, uris=[]):
    if len(name) == 0:
        raise Exception("Will not add a tune with an empty name")

    cur = dbcon.cursor()
    try:
        cur.execute("INSERT INTO tune(name) VALUES(?);", (name,))
        tune_id = resolve_id(dbcon, name)
        for uri in uris:
            cur.execute(
                "INSERT INTO tune_uri(tune_id, uri) VALUES(?,?);",
                (tune_id, uri)
            )

    except sqlite3.IntegrityError as e:
        if str(e) == "column name is not unique":
            raise Exception("tune \"" + name + "\" is already in db")
        else:
            raise e
    finally:
        cur.close()

add = dbhelp.schema_dependent_fn({
        1: add_v1,
        2: add_v2,
    })

# Remove a tune
def rm_v1(dbcon, nameOrId):
  tuneId = resolve_id(dbcon, nameOrId)

  cur = dbcon.cursor()
  try:
    # This should succeed fairly certainly, given that tuneId
    # refers to an existing row.
    cur.execute("DELETE FROM tune WHERE tuneId = ?;", (tuneId,))
  finally:
    cur.close()

def rm_v2(dbcon, nameOrId):
    tune_id = resolve_id(dbcon, nameOrId)

    cur = dbcon.cursor()
    try:
        # This should succeed fairly certainly, given that tune_id
        # refers to an existing row.
        cur.execute("DELETE FROM tune WHERE tune_id = ?;", (tune_id,))
    finally:
        cur.close()

rm = dbhelp.schema_dependent_fn({
        1: rm_v1,
        2: rm_v2,
    })

def details_v1(dbcon, nameOrId):
  tuneId = resolve_id(dbcon, nameOrId)
  cur = dbcon.cursor()
  try:
    cur.execute("SELECT tuneId, name, thesessionUri FROM tune " +
                "WHERE tuneId = ?;",
                (tuneId,))
    row = cur.fetchone()
    thesession_uri = row[2]
    if thesession_uri == "":
      uris = []
    else:
      uris = [thesession_uri]
    return { "tune_id": row[0],
             "name"          : row[1],
             "uris"          : uris,
      }
  finally:
    cur.close()

def details_v2(dbcon, name_or_id):
    tune_id = resolve_id(dbcon, name_or_id)
    cur = dbcon.cursor()
    try:
        cur.execute("SELECT name FROM tune WHERE tune_id = ?;",
                    (tune_id,))
        result = {
            "tune_id": tune_id,
            "name" : cur.fetchone()[0],
            }
        cur.execute("SELECT uri FROM tune_uri WHERE tune_id = ?;",
                    (tune_id,))
        result["uris"] = [row[0] for row in cur.fetchall()]
        return result
    finally:
        cur.close()

# Get details of a tune. Results will be dictionaries of the form
#
# {
#   "tune_id": integer,
#   "name": tune name,
#   "uris" : list of strings
# }
details = dbhelp.schema_dependent_fn({
        1: details_v1,
        2: details_v2,
    })

def _search_glob_str(part_of_name):
    # Poor man's emulation of some very specific regexp features:

    # Implicitly add an asterisk to start, unless it was suppressed
    # by starting the search string with a "^".
    if part_of_name[:1] == "^":
        # Still have to clear out the caret.
        part_of_name = part_of_name[1:]
    elif part_of_name[:1] != "*":
        part_of_name = "*" + part_of_name

    # Similar thing with end of search string:
    if part_of_name[-1:] == "$":
        part_of_name = part_of_name[:-1]
    elif part_of_name[-1:] != "*":
        part_of_name = part_of_name + "*"

    # Try to do a case-insensitive search
    part_of_name = part_of_name.lower()

    return part_of_name

def _search_row_to_dict(row):
    d = {
        "tune_id": row[0],
        "tune_name": row[1],
    }
    return d

# Returns a list of dictionaries, with tune names and tune ids in
# them.
def search_by_name_v1(dbcon, partOfName):
  partOfName = _search_glob_str(partOfName)

  cur = dbcon.cursor()
  try:
    cur.execute("SELECT tuneId,name FROM tune WHERE lower(name) GLOB ?" +
                " ORDER BY name;",
                (partOfName,))
    return list(map(_search_row_to_dict, cur.fetchall()))
  finally:
    cur.close()

def search_by_name_v2(dbcon, part_of_name):
    part_of_name = _search_glob_str(part_of_name)
    cur = dbcon.cursor()
    try:
        search_query = """
SELECT tune_id,name FROM tune WHERE lower(name) GLOB ? ORDER BY name;
"""
        cur.execute(search_query, (part_of_name,))
        return [_search_row_to_dict(row) for row in cur.fetchall()]
    finally:
        cur.close()

search_by_name = dbhelp.schema_dependent_fn({
        1: search_by_name_v1,
        2: search_by_name_v2,
    })

# Set direct attributes of a tune
def set_fields_v1(dbcon, nameOrId, attrDict):
  newName           = attrDict.get("name", None)
  new_uris          = attrDict.get("uris", None)

  # Here need to do some adapting, to make the API
  # match the database layout v2.
  if new_uris == None:
    # Let's not change the thesessionUri field
    newThesessionUri = None
  else:
    if len(new_uris) == 0:
      newThesessionUri = ""
    elif len(new_uris) == 1:
      uri_txt = new_uris[0]
      if len(uri_txt) == 0:
        raise Exception("Empty URIs are not allowed")
      newThesessionUri = uri_txt
    else:
      # More than what db v1 can do is being asked.
      msg = "Database layout v1 can not handle multiple tune URIs"
      raise Exception(msg)

  if newName == "":
    raise Exception("Empty name is not allowed")

  tuneId = resolve_id(dbcon, nameOrId)
  cur = dbcon.cursor()

  try:
    if newName != None:
      cur.execute(
        "UPDATE tune SET name = ? WHERE tuneId = ?;",
        (newName, tuneId)
        )
    if newThesessionUri != None:
      cur.execute(
        "UPDATE tune SET thesessionUri = ? WHERE tuneId = ?;",
        (newThesessionUri, tuneId)
        )
  finally:
    cur.close()

def set_fields_v2(dbcon, name_or_id, attr_dict):
    new_name = attr_dict.get("name", None)
    new_uris = attr_dict.get("uris", None)

    tune_id = resolve_id(dbcon, name_or_id)
    cur = dbcon.cursor()
    try:
        if new_name != None:
            cur.execute(
                "UPDATE tune SET name = ? WHERE tune_id = ?;",
                (new_name, tune_id)
            )
        if new_uris != None:
            # Entirely replace the old set of URIs with a new one.
            cur.execute(
                "DELETE FROM tune_uri WHERE tune_id = ?;",
                (tune_id,)
            )
            for uri in new_uris:
                cur.execute(
                    "INSERT INTO tune_uri(tune_id, uri) VALUES(?, ?);",
                    (tune_id, uri)
                )
    finally:
        cur.close()

set_fields = dbhelp.schema_dependent_fn({
        1: set_fields_v1,
        2: set_fields_v2,
    })

def add_uri_v1(dbcon, name_or_id, uri):
    if len(uri) == 0:
        raise Exception("Empty uri is not allowed")

    tune_id = resolve_id(dbcon, name_or_id)
    cur = dbcon.cursor()
    try:
        cur.execute("SELECT thesessionUri FROM tune WHERE tuneId = ?;",
                    (tune_id,))
        old_uri = cur.fetchone()[0]
        if len(old_uri) != 0:
            raise Exception("Database layout v1 can only hold one tune URI")
        update_stmt = """
UPDATE tune
SET
    thesessionUri = ?
WHERE
    tuneId = ? AND
    thesessionUri = '';
"""
        cur.execute(update_stmt, (uri, tune_id))
    finally:
        cur.close()

def add_uri_v2(dbcon, name_or_id, uri):
    if len(uri) == 0:
        raise Exception("Empty uri is not allowed")

    tune_id = resolve_id(dbcon, name_or_id)
    cur = dbcon.cursor()
    try:
        # Remove older identical URIs of a tune.
        cur.execute("DELETE FROM tune_uri WHERE tune_id = ? AND uri = ?;",
                    (tune_id, uri))
        # And then add a new one.
        cur.execute("INSERT INTO tune_uri(tune_id, uri) VALUES(?, ?);",
                    (tune_id, uri))
    finally:
        cur.close()

add_uri = dbhelp.schema_dependent_fn({
        1: add_uri_v1,
        2: add_uri_v2,
    })

def rm_uri_v1(dbcon, name_or_id, uri):
    tune_id = resolve_id(dbcon, name_or_id)
    cur = dbcon.cursor()
    try:
        update_stmt = """
UPDATE tune
SET thesessionUri = ''
WHERE
    tuneId = ? AND
    thesessionUri = ?;
"""
        cur.execute(update_stmt, (tune_id, uri))
    finally:
        cur.close()

def rm_uri_v2(dbcon, name_or_id, uri):
    tune_id = resolve_id(dbcon, name_or_id)
    cur = dbcon.cursor()
    try:
        delete_stmt = """
DELETE FROM tune_uri
WHERE
    tune_id = ? AND
    uri = ?;
"""
        cur.execute(delete_stmt, (tune_id, uri))
    finally:
        cur.close()

rm_uri = dbhelp.schema_dependent_fn({
        1: rm_uri_v1,
        2: rm_uri_v2,
    })
