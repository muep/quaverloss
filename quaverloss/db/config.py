# quaverloss/db/config.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

def get(dbcon, key):
  cur = dbcon.cursor()
  try:
    cur.execute("SELECT value FROM config WHERE key = ?;", (key,))
    row = cur.fetchone()
    if row == None:
      return ""
    return row[0]
  finally:
    cur.close()

def set(dbcon, key, value):
  cur = dbcon.cursor()
  try:
    cur.execute("DELETE FROM config WHERE key = ?;", (key,))
    cur.execute("INSERT INTO config(key, value) VALUES(?,?);",
                (key, value))
  finally:
    cur.close()
