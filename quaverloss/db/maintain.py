# quaverloss/db/maintain.py
# coding: utf-8
#
# Database maintenance operations.
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

# This module is mostly intended to consist of a database reset
# mechanism, database schema update mechanisms

import pkgutil

schema_version = 2

def schema_version_in_db(dbcon):
    cur = dbcon.cursor()
    try:
        cur.execute("SELECT value FROM config WHERE key = 'schema';")
        return int(cur.fetchone()[0])
    finally:
        cur.close()

def dump(dbcon, where_to):
  for line in dbcon.iterdump():
    print(line, file=where_to)

def reset_db(dbcon, schema_ver = schema_version):
    script_name = "db_v{}.sql".format(schema_ver)
    script = pkgutil.get_data(__name__, script_name).decode("utf-8")

    cur = dbcon.cursor()
    try:
        cur.execute("PRAGMA foreign_keys = OFF;")
        cur.executescript(script)
        cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute("INSERT INTO config(key,value) VALUES('schema', ?);",
                    (schema_ver,))
    finally:
        cur.close()

def upgrade_v1_v2(dbcon):
    assert schema_version_in_db(dbcon) == 1

    script = pkgutil.get_data(__name__, "db_v1_v2.sql").decode("utf-8")
    cur = dbcon.cursor()
    try:
        cur.executescript(script)
    finally:
        cur.close()

def upgrade(dbcon, target_ver=schema_version, verbose=False):
    if verbose:
        print("Upgrading to schema version {}".format(target_ver))

    try:
        if target_ver <= 1:
            return

        if schema_version_in_db(dbcon) == 1:
            upgrade_v1_v2(dbcon)

        if target_ver <= 2:
            return
    except Exception:
        print("Exception while performing a database upgrade")
    else:
        if verbose:
            print("Upgrade operation succeeded")
