-- Copyright 2013 Joonas Sarajärvi
--
-- This file is part of Quaverloss.
--
-- Quaverloss is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- Quaverloss is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public
-- License along with Quaverloss. In source copies of Quaverloss, it is
-- usually included in files named COPYING and COPYING.LESSER. The
-- license is also available at <http://www.gnu.org/licenses/>.

-- SQLite database creation statements for the tune logger.

DROP TABLE IF EXISTS tune;
DROP TABLE IF EXISTS tune_uri;
DROP TABLE IF EXISTS success_level;
DROP TABLE IF EXISTS instrument;
DROP TABLE IF EXISTS practice_session;
DROP TABLE IF EXISTS practice_session_bpm;
DROP TABLE IF EXISTS config;

-- Name is the only mandatory piece of information about
-- tunes.
CREATE TABLE tune (
    tune_id INTEGER PRIMARY KEY UNIQUE,
    name TEXT UNIQUE NOT NULL
);

-- The user can mark URIs from which the tune can be found
-- in some form.
CREATE TABLE tune_uri (
    tune_uri_id INTEGER PRIMARY KEY UNIQUE,
    tune_id INTEGER NOT NULL
        -- Likely ok to let the removal of tunes just
        -- cascade to the URIs.
        REFERENCES tune(tune_id) ON DELETE CASCADE,
    uri TEXT NOT NULL DEFAULT ''
);

-- A bit cumbersome way to record how succesfully a tune
-- was played in a practice session.
CREATE TABLE success_level (
    success_level_id INTEGER PRIMARY KEY UNIQUE,
    -- User is intended to use this when referring to levels
    name TEXT UNIQUE NOT NULL,
    -- This is used for recognizing which levels are "good"
    -- and which are less good. A higher number will signify
    -- a more desirable outcome. The absolute value of this is not
    -- intended to be used for anything.
    niceness INTEGER UNIQUE NOT NULL,
    description TEXT NOT NULL DEFAULT ''
);

CREATE TABLE instrument (
    instrument_id INTEGER PRIMARY KEY UNIQUE,
    name TEXT UNIQUE NOT NULL
);

-- Progress in practicing is mainly intended to be recorded here
CREATE TABLE practice_session (
    practice_session_id INTEGER PRIMARY KEY UNIQUE,
    -- Here we actually restrict deletions to avoid accidentally
    -- losing practice history.
    tune_id INTEGER NOT NULL
        REFERENCES tune(tune_id) ON DELETE RESTRICT,
    instrument_id INTEGER NOT NULL
        REFERENCES instrument(instrument_id) ON DELETE RESTRICT,
    success_level_id INTEGER NOT NULL
        REFERENCES success_level(success_level_id) ON DELETE RESTRICT,
    practice_stamp INTEGER NOT NULL,
    remarks TEXT NOT NULL DEFAULT ''
);

CREATE TABLE practice_session_bpm (
    practice_session_id INTEGER UNIQUE NOT NULL
        REFERENCES practice_session(practice_session_id)
        ON DELETE CASCADE,
    bpm REAL NOT NULL
);

-- These might be better stored in a configuration file.
CREATE TABLE config (
  config_id INTEGER PRIMARY KEY UNIQUE,
  key TEXT UNIQUE NOT NULL,
  value TEXT NOT NULL
);
