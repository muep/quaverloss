# quaverloss/db/jsondump.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import quaverloss.dbhelp as dbhelp

# Build a large dictionary that describes the entire contents of the
# database. It is intended that the output from this function can be
# fed to json.dumps, for getting a more database agnostic dump of
# database contents.
def dict_dump_v1(dbcon):
  # Result dictionary, initially initialized to empty values.
  #
  # At least for now, the config table is not included.
  result = {
    "tune" : [],
    "successLevel" : [],
    "instrument" : [],
    "practiceSession" : [],
    "schema_version" : 1,
    }

  cur = dbcon.cursor()
  try:
    cur.execute("SELECT name, thesessionUri FROM tune;")
    for row in cur:
      tune = {
        "name" : row[0],
        "thesessionUri" : row[1]
        }
      result["tune"].append(tune)

    cur.execute("SELECT name, niceness, description FROM successLevel;")
    for row in cur:
      slevel = {
        "name" : row[0],
        "niceness" : row[1],
        "description" : row[2]
        }

      result["successLevel"].append(slevel)

    cur.execute("SELECT name FROM instrument;")
    for row in cur:
      instrument = {
        "name" : row[0]
        }
      result["instrument"].append(instrument)

    query = """
SELECT
  tune.name,
  instrument.name,
  successLevel.name,
  practiceSession.practiceStamp,
  practiceSession.remarks,
  practiceSession.bpm
FROM
  practiceSession
  JOIN tune
    ON practiceSession.tuneId = tune.tuneId
  JOIN successLevel
    ON practiceSession.successLevelId = successLevel.successLevelId
  JOIN instrument
    ON practiceSession.instrumentId = instrument.instrumentId;
"""
    cur.execute(query)
    for row in cur:
      session = {
        "tune" : row[0],
        "instrument" : row[1],
        "successLevel" : row[2],
        "practiceStamp" : row[3],
        "remarks" : row[4],
        "bpm" : row[5]
        }
      result["practiceSession"].append(session)
    return result
  finally:
    cur.close()

# Build a large dictionary that describes the entire contents of the
# database. It is intended that the output from this function can be
# fed to json.dumps, for getting a more database agnostic dump of
# database contents.
def dict_dump_v2(dbcon):
    # Result dictionary, initially initialized to empty values.
    #
    # At least for now, the config table is not included.
    result = {
        "tune" : [],
        "success_level" : [],
        "instrument" : [],
        "practice_session" : [],
        "schema_version" : 2,
    }

    cur = dbcon.cursor()
    try:
        cur.execute("SELECT name FROM tune;")
        for row in cur:
            tune_name = row[0]
            cur2 = dbcon.cursor()
            try:
                uri_query = """
SELECT tune_uri.uri
FROM
    tune JOIN tune_uri
        ON tune.tune_id = tune_uri.tune_id
WHERE tune.name = ?;
            
"""
                cur2.execute(uri_query, (tune_name,))
                uris = [row2[0] for row2 in cur2]
                tune = {
                    "name" : tune_name,
                    "uris" : uris,
                }
                result["tune"].append(tune)
            finally:
                cur2.close()

        cur.execute(
            "SELECT name, niceness, description FROM success_level;"
        )
        for row in cur:
            slevel = {
                "name" : row[0],
                "niceness" : row[1],
                "description" : row[2]
            }

            result["success_level"].append(slevel)

        cur.execute("SELECT name FROM instrument;")
        for row in cur:
            instrument = {
                "name" : row[0]
            }
            result["instrument"].append(instrument)

        psession_query = """
SELECT
    practice_session.practice_session_id,
    tune.name,
    instrument.name,
    practice_session.practice_stamp,
    practice_session.remarks,
    success_level.name,
    practice_session_bpm.bpm
FROM
    practice_session
    JOIN tune ON
        practice_session.tune_id = tune.tune_id
    JOIN instrument ON
        practice_session.instrument_id = instrument.instrument_id
    JOIN success_level ON
        practice_session.success_level_id = success_level.success_level_id
    LEFT OUTER JOIN practice_session_bpm ON
        practice_session_bpm.practice_session_id =
        practice_session.practice_session_id
"""
        cur.execute(psession_query)
        for row in cur:
            practice_session_id = row[0]
            session = {
                "tune" : row[1],
                "instrument" : row[2],
                "practice_stamp" : row[3],
                "remarks" : row[4],
                "success_level": row[5],
            }
            if row[6] != None:
                session["bpm"] = row[6]

            result["practice_session"].append(session)
        return result
    finally:
        cur.close()

dict_dump = dbhelp.schema_dependent_fn({
        1: dict_dump_v1,
        2: dict_dump_v2,
    })
