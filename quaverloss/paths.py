# quaverloss/paths.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

# A very minimal module that tries to get us a suitable data
# directory, trying to follow the XDG Base Directory spec as published
# at
#
#  http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
#
# and possibly some other conventions on platforms where the above
# specification is not easily applicable.
#
# Since the configuration is currently stored in along the main
# database, we do not reall need the config directory and our needs
# are fairly modest:

import datetime
import os
import os.path
import platform

import quaverloss.datehelp

def user_homedir():
  return os.environ.get("HOME")

def xdg_user_datadir():
  fback = user_homedir() + "/.local/share"
  return os.environ.get("XDG_DATA_HOME", fback)

def windows_appdata():
  # Not sure about which versions of Microsoft Windows provide this
  # with a sane value. Feedback and suggestions are welcome.
  return os.environ.get("APPDATA")

def our_datadir():
  if platform.system() == "Windows":
    return windows_appdata() + "\\quaverloss"

  # Probably a bad assumption that the XDG scheme suits every
  # non-Windows case.
  return xdg_user_datadir() + "/quaverloss"

def ensure_dir(path):
  if not os.path.exists(path):
    os.makedirs(path)

def dbfile_path():
  return os.path.normpath(our_datadir() + "/quaverloss.sqlite3")

def dumpautofile_path(stamp):
  # For the convenience of the implementation, the pretty timestamp
  # are (for now) just UTC ones.
  dt = datetime.datetime.fromtimestamp(stamp, quaverloss.datehelp.utc)
  stampTxt = dt.strftime("%Y-%m-%dT%H%M%S")
  p = our_datadir() + "/quaverloss_" + stampTxt + ".sql"
  return os.path.normpath(p)
