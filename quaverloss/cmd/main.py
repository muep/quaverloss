# quaverloss/cmd/main.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import sqlite3
import sys
import time

import quaverloss.cmd.config       as cmdconfig
import quaverloss.cmd.db           as cmddb
import quaverloss.cmd.instrument   as cmdinstrument
import quaverloss.cmd.tune         as cmdtune
import quaverloss.cmd.practice     as cmdpractice
import quaverloss.cmd.successlevel as cmdsuccesslevel
import quaverloss.db.maintain      as dbmaint
import quaverloss.dbhelp           as dbhelp

# For maintaining application state. Currently filled in the main
# function.
class Context:
    pass

def mainParser():
    ap = argparse.ArgumentParser()
    ap.add_argument("-v", "--verbose",
                    help="Additional textual output",
                    action="store_true")

    ap.add_argument("--db-location",
                    help="Override the database location",
                    dest="dbLocation")

    saps = ap.add_subparsers()
    subp = saps.add_parser("instrument",
                           help="manage your instruments")
    cmdinstrument.prepare_parser(subp)

    subp = saps.add_parser("tune", help="manage tunes")
    cmdtune.prepare_parser(subp)

    subp = saps.add_parser("practice",
                           help="mark and review practice sessions")
    cmdpractice.prepare_parser(subp)

    subp = saps.add_parser("successlevel", help="manage success levels")
    cmdsuccesslevel.prepare_parser(subp)

    subp = saps.add_parser("config", help="configure stuff")
    cmdconfig.prepare_parser(subp)

    subp = saps.add_parser("db", help="database maintenance")
    cmddb.prepare_parser(subp)

    return ap

def main(args):
    def noprint(txt):
        pass

    app = Context()

    app.invocation_stamp = int(time.time())
    app.argparser = mainParser()
    app.args = app.argparser.parse_args(args)

    if not hasattr(app.args, "func"):
        app.argparser.print_help()
        exit(0)

    # Handle verbosity
    app.print_info = noprint
    app.print_results = print
    if app.args.verbose:
        app.print_info = print

    app.dbcon = dbhelp.open_db(path=app.args.dbLocation,
                               print_info=app.print_info)

    try:
        app.args.func(app)
    except dbhelp.SchemaVersionMismatch as e:
        print("Could not perform the operation because of")
        print("unsupported version of database layout: {}".format(
                e.detected_schema_version
            )
        )
        print("Latest layout version supported: {}".format(
                dbmaint.schema_version
            )
        )
        if e.upgrade_could_help:
            print("Consider running the db upgrade command.")
        else:
            print("You need a more recent version of quaverloss to")
            print("access this database.")
    except Exception as e:
        app.dbcon.rollback()
        print("database changes aborted")
        raise e
    else:
        app.dbcon.commit()

def main_without_exceptionhandling():
    main(sys.argv[1:])

def main_with_printexceptions():
    try:
        main(sys.argv[1:])
    except Exception as e:
        print(sys.argv[0] + " encountered an error:\n" + str(e))
        raise e

if __name__ == "__main__":
    main_with_printexceptions()
