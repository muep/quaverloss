# quaverloss/cmd/tune.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import datetime
import io

import quaverloss.db.practicesessions as psessions
import quaverloss.db.tunes            as tunes
import quaverloss.datehelp            as datehelp

def format_note(notedict):
  epoch = datetime.datetime(1970, 1, 1, tzinfo = datetime.timezone.utc)
  secondsSinceEpoch = notedict["noteStamp"]
  ndatetime = epoch + datetime.timedelta(seconds=secondsSinceEpoch)
  ldatetime = ndatetime.astimezone()
  tname = notedict["tuneName"]

  with io.StringIO() as sio:
    print("About " + tname +", on " + str(ldatetime) + ":", file=sio)
    print(notedict["noteText"], file=sio)

    return sio.getvalue()

def perform_show(app):
  args = app.args

  details = tunes.details(app.dbcon, args.tune)
  practices = psessions.recents(app.dbcon,
                                tune=args.tune,
                                instrument=args.instrument)
  app.print_results("Details of " + args.tune + ":")
  uris = details["uris"]

  if len(uris) > 0:
    app.print_results("  available at: ")
    for uri in uris:
      app.print_results("   " + uri)

  if len(practices) > 0:
    app.print_results(" Recent practice sessions:")
    for p in practices:
      stampTxt = datehelp.stamp_to_datestr(p["practice_stamp"])
      app.print_results("  on " + stampTxt +
                         ", with " + p["instrument_name"] +
                         " (" + p["success_level_name"] + " at " +
                         str(p["practice_bpm"]) + " bpm)")
  else:
    app.print_results(" No practice records of this tune")

def perform_search(app):
  args = app.args

  results = tunes.search_by_name(app.dbcon, args.name)
  result_cnt = len(results)

  tword = "tunes"
  if result_cnt == 1:
    tword = "tune"

  app.print_results("Found " + str(result_cnt) + " " + tword +" with " +
                    args.name + " in their names:")
  for result in results:
    tune_id_suffix = ""
    if app.args.show_ids:
      tune_id_suffix = " (" + str(result["tune_id"]) + ")"

    tune_name = result["tune_name"]
    app.print_results("  " + tune_name + tune_id_suffix)

def perform_add(app):
    tunes.add(app.dbcon, app.args.name)
    app.print_info("adding tune \"" + app.args.name + "\"")

def perform_rm(app):
  tunes.rm(app.dbcon, app.args.name)
  app.print_info("removing tune \"" + app.args.name + "\"")

def perform_edit(app):
  newFields = {
    "name":          app.args.new_name,
    }

  tunes.set_fields(app.dbcon, app.args.name, newFields)

def perform_uri(app):
    fns = {
        "add": tunes.add_uri,
        "rm": tunes.rm_uri
        }
    fn = fns[app.args.operation]
    fn(app.dbcon, app.args.tune_name, app.args.uri)

def prepare_parser(ap):
    subs = ap.add_subparsers()

    show_parser = subs.add_parser("show",
                                  help="show details of a tune")
    show_parser.add_argument("tune",
                             help="name of tune to show")
    show_parser.add_argument("--instrument",
                             help="focus on a single instrument")
    show_parser.set_defaults(func=perform_show)

    search_parser = subs.add_parser("search",
                                    help="search for a tune by name")
    search_parser.add_argument("name", help="part of name of tune")
    search_parser.add_argument("--show-ids", "-n",
                               help="display internal tune ids",
                               action="store_true")
    search_parser.set_defaults(func=perform_search)

    add_parser = subs.add_parser("add", help="add a tune")

    add_parser.add_argument("name", help="name of the tune")
    add_parser.set_defaults(func=perform_add)

    rm_parser = subs.add_parser("rm", help="remove a tune")
    rm_parser.add_argument("name", help="name of tune to be removed")
    rm_parser.set_defaults(func=perform_rm)

    edit_parser = subs.add_parser("edit", help="edit details of a tune")
    edit_parser.add_argument("name", help="name of tune to edit")
    edit_parser.add_argument("--name",
                             help="rename a tune",
                             dest="new_name",
                             metavar="NEW_NAME")
    edit_parser.set_defaults(func=perform_edit)

    uri_parser = subs.add_parser("uri", help="manage tune URIs")
    uri_parser.add_argument("operation",
                            choices=("add","rm"),
                            help="operation to perform")
    uri_parser.add_argument("tune_name", help="name of tune whose URIs to manage")
    uri_parser.add_argument("uri", help="URI text to add or remove")
    uri_parser.set_defaults(func=perform_uri)
