# quaverloss/cmd/successlevel.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import quaverloss.db.successlevels as sls

def perform_add(app):
  nice_txt = app.args.niceness
  try:
    niceness = int(nice_txt)
  except ValueError:
    raise Exception("Niceness level must be an integer")

  desc = app.args.description
  if desc == None:
    desc = ""
  sls.add(app.dbcon, app.args.name, niceness, desc)

def perform_rm(app):
  sls.rm(app.dbcon, app.args.name)

def perform_list(app):
  lvls = sls.get_all(app.dbcon)
  for lvl in lvls:
    # todo: improve this formatting
    app.print_results("  " + lvl["name"])

def perform_show(app):
  sl = sls.by_name(app.dbcon, app.args.name)
  app.print_results("         name: " + sl["name"])
  app.print_results("  description: " + sl["description"])
  app.print_results("     niceness: " + str(sl["niceness"]))

def perform_edit(app):
  args = app.args

  newFields = {
      "name"        : args.new_name,
      "niceness"    : int(args.niceness) if args.niceness else None,
      "description" : args.description
      }

  sls.set_fields(
    app.dbcon,
    args.name,
    newFields
  )

def prepare_parser(ap):
  subs = ap.add_subparsers()

  add_parser = subs.add_parser("add", help="define a new success level")
  add_parser.add_argument("name", help="name of level to be added")
  add_parser.add_argument("niceness",
                          help="numeric description of quality")
  add_parser.add_argument("--description",
                          help="description for success level")
  add_parser.set_defaults(func=perform_add)

  rm_parser = subs.add_parser("rm",
                              help="remove an existing success level")
  rm_parser.add_argument("name")
  rm_parser.set_defaults(func=perform_rm)

  list_parser = subs.add_parser("list",
                                help="list current success levels")
  list_parser.set_defaults(func=perform_list)

  show_parser = subs.add_parser("show",
                                help="show details of a success level")
  show_parser.add_argument("name", help="name of success level to show")
  show_parser.set_defaults(func=perform_show)

  edit_parser = subs.add_parser("edit",
                                help="edit a success level")
  edit_parser.add_argument("name", help="name of success level to edit")
  edit_parser.add_argument("--name",
                           help="rename a success level",
                           dest="new_name")
  edit_parser.add_argument("--description",
                           help="set description of a success level")
  edit_parser.add_argument("--niceness",
                           help="set the quality measure of a level")
  edit_parser.set_defaults(func=perform_edit)
