# coding: utf-8
# quaverloss/cmd/db.py
#
# Command line interface for performing operations exposed in
# quaverloss/db/maintain.py.
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

import json
import sys

import quaverloss.db.jsondump as jsondump
import quaverloss.db.maintain as qdb
import quaverloss.paths as qpaths

def perform_reset(app):
    qdb.reset_db(app.dbcon)

def perform_info(app):
    max_ver = qdb.schema_version
    det_ver = qdb.schema_version_in_db(app.dbcon)

    print("Latest supported db schema version: {}".format(max_ver))
    print("Schema version in database: {}".format(det_ver))

def perform_upgrade(app):
    if app.args.schema_version != None:
        target_ver = app.args.schema_version
    else:
        target_ver = qdb.schema_version

    qdb.upgrade(app.dbcon, verbose=app.args.verbose, target_ver=target_ver)

def perform_dump(app):
    args = app.args
    dbcon = app.dbcon

    if args.file != None:
        with open(args.file, "w") as f:
            qdb.dump(dbcon, f)
    elif args.autofile:
        with open(qpaths.dumpautofile_path(app.invocation_stamp), "w") as f:
            qdb.dump(dbcon, f)
    else:
        qdb.dump(dbcon, sys.stdout)

def perform_jsondump(app):
    args = app.args
    dbcon = app.dbcon

    d = jsondump.dict_dump(dbcon)
    if args.pretty:
        json.dump(
            d, sys.stdout,
            separators=(',', ': '),
            indent=4
        )
    else:
        json.dump(d, sys.stdout)

def prepare_parser(ap):
    subs = ap.add_subparsers()

    info_parser = subs.add_parser(
        "info",
        help="show database information"
    )
    info_parser.set_defaults(func=perform_info)

    upgrade_parser = subs.add_parser(
        "upgrade",
        help="upgrade the database of an older quaverloss version"
    )
    upgrade_parser.add_argument(
        "--schema-version",
        help="target schema version",
        type=int
    )
    upgrade_parser.set_defaults(func=perform_upgrade)

    dump_parser = subs.add_parser("dump",
                                  help="print a textual database dump")
    # Destination group
    dump_dg = dump_parser.add_mutually_exclusive_group(required=True)
    dump_dg.add_argument("--file",
                         help="file to which to write the dump")
    dump_dg.add_argument("--stdout",
                         action="store_true",
                         help="dump to standard output")
    dump_dg.add_argument("--autofile",
                         help="decide dump filename automatically",
                         action="store_true")
    dump_parser.set_defaults(func=perform_dump)

    reset_parser = subs.add_parser("reset", help="reset the database")
    reset_parser.set_defaults(func=perform_reset)

    jsondump_parser = subs.add_parser(
        "jsondump",
        help="dump database contents in JSON form"
    )
    jsondump_parser.add_argument(
        "--pretty",
        action="store_true",
        help="pretty-format the output"
    )
    jsondump_parser.set_defaults(func=perform_jsondump)
