# quaverloss/cmd/practice.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import quaverloss.db.practicesessions as sessions
import quaverloss.datehelp            as datehelp

def perform_add(app):
  args = app.args
  # TODO: Provide some sane way to input timestamps
  stamp = args.time
  if stamp == None:
    stamp = app.invocation_stamp

  remarks = args.remarks
  if remarks == None:
    remarks = ""

  try:
    bpm = float(args.bpm)
  except ValueError:
    raise Exception("Could not accept bpm of \"" + args.bpm + "\"")

  sessions.add(
    app.dbcon,
    args.tune,
    stamp,
    args.instrument,
    args.success,
    remarks,
    bpm
    )

def perform_rm(app):
    args = app.args
    sessions.rm(app.dbcon, args.practice_session_id)

def perform_list(app):
  args = app.args

  summaries = sessions.summary_all(app.dbcon)
  for summary in summaries:
    stamp_key = "practice_session_practice_stamp"
    stamp_txt = datehelp.stamp_to_datestr(summary[stamp_key])
    bpm = summary["practice_session_bpm"]
    app.print_results("On " + stamp_txt + ":")
    app.print_results("  practiced " + summary["tune_name"], end="")
    app.print_results(" with " + summary["instrument_name"])
    app.print_results("  got " + summary["success_level_name"], end="")
    app.print_results(" results at " + str(bpm), end="")
    app.print_results(" bpm", end="\n\n")

def perform_recent(app):
  args = app.args

  # TODO: Format
  insts = args.instrument
  if insts == None:
    insts = [ None ]

  tuns = args.tune
  if tuns == None:
    tuns = [ None ]

  practices = []
  for inst in insts:
    for tune in tuns:
      practices += sessions.recents(app.dbcon,
                                    tune=tune,
                                    instrument=inst,
                                    maxCount=args.maxCount)
  if len(insts) * len(tuns) > 1:
    # Need to do some additional sorting and limit result lengths
    # again.
    #
    # TODO: This would be better to handle inside the practicesessions
    # module.
    practices.sort(key=lambda p: -p["practice_stamp"])
    practices = practices[:args.maxCount]

  for p in practices[::-1]:
    stampTxt = datehelp.stamp_to_datestr(p["practice_stamp"])

    id_txt = ""
    if args.with_session_ids:
      id_txt = "{}: ".format(p["practice_session_id"])

    txt = "{idt}{stamp}: {tune} with {instrument}".format(
        idt=id_txt,
        stamp=stampTxt,
        tune=p["tune_name"],
        instrument=p["instrument_name"]
    )

    sl_name = p["success_level_name"]
    bpm = p["practice_bpm"]

    addons = []
    if sl_name != None:
        addons.append(sl_name)
    if bpm != None and bpm > 0:
        addons.append("at {} bpm".format(bpm))
    if addons != []:
        txt += " (" + " ".join(addons) + ")"

    app.print_results(txt)

def perform_suggest(app):
  def print_tunelist(description, tunelist):
    app.print_results(description)
    for t in tunelist:
      tname = t["tune_name"]
      iname = t["instrument_name"]
      stamp = t["last_practice_stamp"]
      if stamp == None:
        app.print_results("  " + tname + " is unplayed")
      else:
        stampTxt = datehelp.stamp_to_datestr(stamp)
        app.print_results("  " + tname + " was last played with " +
                          iname + " at " + stampTxt)

  args = app.args

  new_tunes = sessions.suggest_news(
    app.dbcon,
    args.instrument,
    maxCount = args.max_recent_count
    )

  new_tune_ids = set(map(lambda nt: nt["tune_id"], new_tunes))

  old_tunes = sessions.suggest_olds(
    app.dbcon,
    args.instrument,
    maxCount = args.max_neglected_count
    )

  old_tunes = list(filter(lambda ot: not ot["tune_id"] in new_tune_ids,
                          old_tunes))

  if len(new_tunes) > 0:
    print_tunelist("Recently practiced:", new_tunes)
  if len(old_tunes) > 0:
    print_tunelist("Recently neglected:", old_tunes)

def perform_unpracticed(app):
  tunelist = sessions.unpracticeds2(app.dbcon, app.args.instrument)
  for tune in tunelist:
    app.print_results(tune["tune_name"])

def prepare_parser(ap):
  subs = ap.add_subparsers()

  add_parser = subs.add_parser("add",
                               help="mark a new practice session")
  add_parser.add_argument(
    "--instrument",
    metavar="INSTRUMENT_NAME",
    required=True,
    help="name of instrument used in session"
  )

  add_parser.add_argument(
    "--tune",
    metavar="TUNE_NAME",
    required=True,
    help="name of tune that was played"
  )

  add_parser.add_argument(
    "--success",
    metavar="LEVEL",
    required=True,
    help="level of success achieved while practicing"
  )

  add_parser.add_argument(
    "--bpm",
    required=True,
    help="tempo that was used while practicing"
  )

  stampnote = " (unfortunately just unix timestamps, for now)"

  add_parser.add_argument(
    "--time",
    required=False,
    metavar="WHEN",
    help="time at which practicing occurred" + stampnote
  )

  add_parser.add_argument(
    "--remarks",
    required=False,
    help="free-form notes about how the practicing went"
  )

  add_parser.set_defaults(func=perform_add)

  rm_parser = subs.add_parser("rm", help="remove a practice session")
  rm_parser.add_argument(
    "practice_session_id",
    type=int,
    help="numeric id of practice session to remove"
  )
  rm_parser.set_defaults(func=perform_rm)

  list_parser = subs.add_parser("list", help="list practice sessions")
  list_parser.set_defaults(func=perform_list)

  recent_parser = subs.add_parser("recent",
                                  help="display recent practice activity")
  recent_parser.add_argument("--with-session-ids",
                             action="store_true",
                             help="display internal practice session ids")
  recent_parser.add_argument("--instrument",
                             action="append",
                             help="limit listing to a specific insturment")
  recent_parser.add_argument("--tune",
                             action="append",
                             help="limit listing to a specific tune")
  recent_parser.add_argument("--max-count",
                             dest="maxCount",
                             metavar="N",
                             default=10,
                             type=int,
                             help="how many sessions to show, at maximum")
  recent_parser.set_defaults(func=perform_recent)


  suggest_parser = subs.add_parser("suggest",
                                   help="display a list of tunes to practice")
  suggest_parser.add_argument("--instrument",
                              help="limit listing to a specific instrument")

  suggest_parser.add_argument("--max-recent-count",
                              type=int, default=5,
                              dest="max_recent_count")

  suggest_parser.add_argument("--max-neglected-count",
                              type=int, default=5,
                              dest="max_neglected_count")
  suggest_parser.set_defaults(func=perform_suggest)

  unpracticed_parser = subs.add_parser("unpracticed",
                                       help="list unpracticed tunes")
  unpracticed_parser.add_argument("--instrument",
                                  help="focus on a single instrument")
  unpracticed_parser.set_defaults(func=perform_unpracticed)
