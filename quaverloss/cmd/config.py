# quaverloss/cmd/config.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import quaverloss.db.config as config

def perform_set(app):
  config.set(app.dbcon, app.args.key, app.args.value)
  app.print_info("set " + app.args.key + " to value " + app.args.value)

def perform_get(app):
  app.print_results(config.get(app.dbcon, app.args.key))

def prepare_parser(ap):
  subs = ap.add_subparsers()

  get_parser = subs.add_parser("get", help="inspect a config value")
  get_parser.add_argument("key")
  get_parser.set_defaults(func=perform_get)

  set_parser = subs.add_parser("set", help="set a config value")
  set_parser.add_argument("key")
  set_parser.add_argument("value")
  set_parser.set_defaults(func=perform_set)
