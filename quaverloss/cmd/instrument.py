# quaverloss/cmd/instrument.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import quaverloss.db.instruments as instruments

def perform_add(app):
  instruments.add(app.dbcon, app.args.name)

def perform_rm(app):
  instruments.rm(app.dbcon, app.args.name)

def perform_list(app):
  insts = instruments.get_all(app.dbcon)
  for inst in insts:
    app.print_results("  " + inst["instrument_name"])

def perform_show(app):
  inst = instruments.by_name(app.dbcon, app.args.name)
  app.print_results("  name: " + inst["name"])

def perform_edit(app):
  args = app.args
  newFields = { "name" : args.new_name }
  instruments.set_fields(app.dbcon, args.name, newFields)

def prepare_parser(ap):
  subs = ap.add_subparsers()

  add_parser = subs.add_parser("add", help="add an instrument")
  add_parser.add_argument("name", help="name of the instrument")
  add_parser.set_defaults(func=perform_add)

  rm_parser = subs.add_parser("rm", help="remove an instrument")
  rm_parser.add_argument("name", help="name of the instrument")
  rm_parser.set_defaults(func=perform_rm)

  list_parser = subs.add_parser("list", help="list your instruments")
  list_parser.set_defaults(func=perform_list)

  show_parser = subs.add_parser("show",
                                help="show details of an instrument")
  show_parser.add_argument("name", help="name of instrument to show")
  show_parser.set_defaults(func=perform_show)

  edit_parser = subs.add_parser("edit",
                                help="edit details of an instrument")
  edit_parser.add_argument("name", help="name of instrument to edit")
  edit_parser.add_argument("--name",
                           help="rename an instrument",
                           dest="new_name",
                           metavar="NEW_NAME")
  edit_parser.set_defaults(func=perform_edit)
