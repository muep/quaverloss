# quaverloss/cmd/performance.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

def perform_add(args):
  args.print_info("adding performance of " + args.name)

def perform_rm(args):
  args.print_info("adding performance of " + args.name)

def prepare_parser(ap):
  subs = ap.add_subparsers()

  add_parser = subs.add_parser("add", help="add a performance")

  add_parser.add_argument("name",   help="name of tune")
  add_parser.add_argument("--bpm",  help="tempo, in beats per minute")
  add_parser.add_argument("--time", help="time at which performance was done")
  add_parser.add_argument("--uri",  help="URI to the performance")
  add_parser.set_defaults(func=perform_add)

  rm_parser = subs.add_parser("remove", aliases=["rm"],
                              help="remove a performance")
  rm_sel_group = rm_parser.add_mutually_exclusive_group(required=True)
  rm_sel_group.add_argument(
    "--id",
    help="database if of performance to remove"
  )
  rm_sel_group.add_argument(
    "--last-by-tune-name",
    metavar="TUNE_NAME",
    help="remove the last added performance of a tune by name"
  )
