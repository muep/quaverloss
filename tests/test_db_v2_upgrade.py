# tests/test_db_v2_upgrade.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import pkgutil
import sqlite3
import unittest

import quaverloss.db.maintain as maintain

def populated_db():
    script = pkgutil.get_data(
        __name__,
        "test_db_v2_upgrade_testdata.sql"
    ).decode()

    dbcon = sqlite3.connect(":memory:")
    try:
        cur = dbcon.cursor()
        cur.execute("PRAGMA foreign_keys = OFF;")
        cur.executescript(script)
        cur.execute("PRAGMA foreign_keys = ON;")
        cur.close()
        return dbcon
    except Exception:
        dbcon.close()
        raise



class DbV2UpgradeTests(unittest.TestCase):
    def setUp(self):
        self.db = populated_db()

    def tearDown(self):
        self.db.close()

    # A fairly trivial test that just ensures that the
    # upgrade script can run with the small chunk of
    # test data we have.
    def test_tunes(self):
        cur = self.db.cursor()
        cur.execute("SELECT tuneId, name, thesessionUri FROM tune;")
        olds = set(cur.fetchall())

        maintain.upgrade_v1_v2(self.db)

        def without_nones(row):
            uri = row[2]
            if uri == None:
                uri = ""
            return (row[0], row[1], uri)

        query2 = """
SELECT
    tune.tune_id,
    tune.name,
    tune_uri.uri
FROM
    tune
        LEFT OUTER JOIN
    tune_uri
        ON tune.tune_id = tune_uri.tune_id;
"""
        cur.execute(query2)
        news = set(map(without_nones, cur.fetchall()))

        self.assertEqual(olds, news)

    def test_instruments(self):
        cur = self.db.cursor()
        cur.execute("SELECT instrumentId, name FROM instrument;")
        olds = set(cur.fetchall())

        maintain.upgrade_v1_v2(self.db)

        cur.execute("SELECT instrument_id, name FROM instrument;")
        news = set(cur.fetchall())

        self.assertEqual(olds, news)

    def test_success_levels(self):
        cur = self.db.cursor()
        cur.execute("SELECT successLevelId, name, niceness, description FROM successLevel;")
        olds = set(cur.fetchall())

        maintain.upgrade_v1_v2(self.db)

        cur.execute("SELECT success_level_id, name, niceness, description FROM success_level;")
        news = set(cur.fetchall())

        self.assertEqual(olds, news)
