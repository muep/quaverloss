# tests/test_practicesessions.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import unittest

import quaverloss.db.instruments as instruments
import quaverloss.db.practicesessions as psessions
import quaverloss.db.successlevels as slevels
import quaverloss.db.tunes as tunes
import quaverloss.dbhelp as qdb

class PracticeSessionsTests(unittest.TestCase):
    def memdb(self):
        return qdb.memdb();

    def setUp(self):
        self.db = self.memdb()

        tunes.add(self.db, "Kesh")
        tunes.add(self.db, "Hag at the churn")
        tunes.add(self.db, "Yellow wattle")
        tunes.add(self.db, "Forgotten reel")

        instruments.add(self.db, "guitar")
        instruments.add(self.db, "whistle")

        slevels.add(self.db, "bad", 50, "")
        slevels.add(self.db, "ok", 100, "")

        psessions.add(self.db, "Kesh",
                      1000, "guitar", "ok", "", 80)
        psessions.add(self.db, "Yellow wattle",
                      1100, "guitar", "bad", "", 80)

        psessions.add(self.db, "Yellow wattle",
                     1200, "whistle", "ok", "", 80)
        psessions.add(self.db, "Hag at the churn",
                     1300, "whistle", "ok", "", 80)



    def tearDown(self):
        self.db.close()

    def test_add1(self):
        psessions.add(self.db, "Forgotten reel",
                     1400, "whistle", "ok", "", 60)

    def test_summary_all(self):
        everything = psessions.summary_all(self.db)
        # There is now quite a lot of data. The code below does
        # not even try to validate all of it.

        self.assertEqual(len(everything), 4)

        # Let's focus a bit on names of tunes
        tuneNames = set(map(lambda s: s["tune_name"], everything))

        # Kesh was practiced, so it should be included
        self.assertIn("Kesh", tuneNames)
        # But Forgotten reel should not be there
        self.assertNotIn("Forgotten reel", tuneNames)

    def test_unpracticeds2(self):
        def names_for_instrument(instName):
            upceds = psessions.unpracticeds2(self.db, instName)
            return set(map(lambda s: s["tune_name"], upceds))

        unpracticed_for_all = names_for_instrument(None)
        self.assertEqual(len(unpracticed_for_all), 1)
        self.assertIn("Forgotten reel", unpracticed_for_all)

        unpracticed_for_whistle = names_for_instrument("whistle")
        self.assertEqual(len(unpracticed_for_whistle), 2)
        self.assertIn("Forgotten reel", unpracticed_for_whistle)
        self.assertIn("Kesh", unpracticed_for_whistle)

    def test_suggests(self):
        # Really just test that it does not crash, for now
        olds = psessions.suggest_olds(self.db, None)
        news = psessions.suggest_news(self.db, None)

        self.assertNotEqual(0, len(olds))
        self.assertNotEqual(0, len(news))

    def test_recents_simple(self):
        recents = psessions.recents(self.db, maxCount = 2)
        self.assertEqual(len(recents), 2)
        self.assertEqual(recents[0]["practice_stamp"], 1300)
        self.assertEqual(recents[1]["practice_stamp"], 1200)

    def test_recents_nomax(self):
        recents = psessions.recents(self.db, maxCount=None)
        self.assertEqual(len(recents), 4)

    def test_rm(self):
        whistled_before = psessions.recents(self.db, tune="Kesh")
        self.assertEqual(len(whistled_before), 1)

        psession_id = whistled_before[0]["practice_session_id"]

        def try_remove():
            psessions.rm(self.db, psession_id)

        try_remove()

        whistled_after = psessions.recents(self.db, tune="Kesh")
        self.assertEqual(0, len(whistled_after))

        self.assertRaises(qdb.IdResolutionError, try_remove)

    def test_details(self):
        keshes = psessions.recents(self.db, tune="Kesh")
        self.assertEqual(len(keshes), 1)
        keshp0 = keshes[0]
        keshp0_id = keshp0["practice_session_id"]

        keshp0_details = psessions.details(self.db, keshp0_id)
        for key in keshp0.keys():
            # We expect the "details" call return the similar
            # values to the "recents" call.
            self.assertEqual(keshp0[key], keshp0_details[key])

class PracticeSessionsTests_schema_v1(PracticeSessionsTests):
    def memdb(self):
        return qdb.memdb(schema_version=1)
