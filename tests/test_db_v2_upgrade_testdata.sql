-- A database dump of a quaverloss version that still
-- used database schema version 1.

BEGIN TRANSACTION;
CREATE TABLE config (
  configId INTEGER PRIMARY KEY UNIQUE,
  key TEXT UNIQUE NOT NULL,
  value TEXT NOT NULL
);
INSERT INTO "config" VALUES(1,'schema','1');
CREATE TABLE instrument (
  instrumentId INTEGER PRIMARY KEY UNIQUE,
  name TEXT UNIQUE NOT NULL
);
INSERT INTO "instrument" VALUES(1,'instrument1');
INSERT INTO "instrument" VALUES(2,'instrument2');
CREATE TABLE practiceSession (
  practiceSessionId INTEGER PRIMARY KEY UNIQUE,
  -- Here we actually restrict deletions to avoid accidentally
  -- losing practice history.
  tuneId INTEGER NOT NULL REFERENCES tune(tuneId) ON DELETE RESTRICT,
  instrumentId INTEGER NOT NULL
    REFERENCES instrument(instrumentId) ON DELETE RESTRICT,
  practiceStamp INTEGER NOT NULL,
  successLevelId INTEGER NOT NULL
    REFERENCES successLevel(successLevelId) ON DELETE RESTRICT,
  remarks TEXT NOT NULL DEFAULT '',
  bpm REAL NOT NULL DEFAULT -1
);
INSERT INTO "practiceSession" VALUES(1,1,1,1383994491,1,'',80.0);
INSERT INTO "practiceSession" VALUES(2,1,2,1383994500,2,'',80.0);
INSERT INTO "practiceSession" VALUES(3,2,1,1383994512,2,'',80.0);
INSERT INTO "practiceSession" VALUES(4,2,2,1383994530,2,'',100.0);
CREATE TABLE successLevel (
  successLevelId INTEGER PRIMARY KEY UNIQUE,
  -- User is intended to use this when referring to levels
  name TEXT UNIQUE NOT NULL,
  -- This is used for recognizing which levels are "good"
  -- and which are less good. A higher number will signify
  -- a more desirable outcome. The absolute value of this is not
  -- intended to be used for anything.
  niceness INTEGER UNIQUE NOT NULL,
  description TEXT NOT NULL DEFAULT ''
);
INSERT INTO "successLevel" VALUES(1,'bad',20,'');
INSERT INTO "successLevel" VALUES(2,'good',40,'');
CREATE TABLE tune (
  tuneId INTEGER PRIMARY KEY UNIQUE,
  name TEXT UNIQUE NOT NULL,
  thesessionUri TEXT NOT NULL DEFAULT ''
);
INSERT INTO "tune" VALUES(1,'tune1','tune1_ts_uri');
INSERT INTO "tune" VALUES(2,'tune2','');
COMMIT;
