# tests/test_maintain.py
# coding: utf-8
#
# Command line interface for performing operations exposed in
# quaverloss/db/maintain.py.
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import unittest

import quaverloss.dbhelp as dbhelp
import quaverloss.db.instruments as instruments
import quaverloss.db.jsondump as jsondump
import quaverloss.db.maintain as maintain
import quaverloss.db.successlevels as slevels
import quaverloss.db.tunes as tunes

def tune_by_name(tune_seq, name):
  for tune in tune_seq:
    if tune["name"] == name:
      return tune
  raise Exception("Did not find the desired tune")

def fill(dbcon):
  tunes.add(dbcon, "foo", uris=["bar"])
  tunes.add(dbcon, "baz")

# A (currently far from exhaustive) test that takes a dictionary dump
# of the database and tests for some details in the output.
#
# TODO: would be nice if this verified a greater number of things.
class MaintainJsonTests(unittest.TestCase):
  def setUp(self):
    self.db = dbhelp.memdb()
    fill(self.db)

  def tearDown(self):
    self.db.close()

  def test_tune_properties(self):
    dump = jsondump.dict_dump(self.db)

    if dump["schema_version"] == 1:
        foo = tune_by_name(dump["tune"], "foo")
        baz = tune_by_name(dump["tune"], "baz")

        self.assertEqual(foo["name"], "foo")
        self.assertEqual(foo["thesessionUri"], "bar")

        self.assertEqual(baz["name"], "baz")
        self.assertEqual(baz["thesessionUri"], "")
    elif dump["schema_version"] == 2:
        foo = tune_by_name(dump["tune"], "foo")
        baz = tune_by_name(dump["tune"], "baz")

        self.assertEqual(foo["name"], "foo")
        self.assertEqual(foo["uris"], ["bar"])

        self.assertEqual(baz["name"], "baz")
        self.assertEqual(baz["uris"], [])
