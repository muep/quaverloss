# tests/test_config.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import unittest

import quaverloss.db.config as config
import quaverloss.dbhelp as qdb

class ConfigTest(unittest.TestCase):
  def setUp(self):
    self.db = qdb.memdb()

    config.set(self.db, "foo", 1)
    config.set(self.db, "bar", "xyzzy")

  def tearDown(self):
    self.db.close()

  def test_get(self):
    foo = config.get(self.db, "foo")
    bar = config.get(self.db, "bar")
    baz = config.get(self.db, "baz")

    self.assertEqual(foo, "1")
    self.assertEqual(bar, "xyzzy")
    self.assertEqual(baz, "")

  def test_set(self):
    old_bar = config.get(self.db, "bar")
    old_baz = config.get(self.db, "baz")

    self.assertEqual(old_bar, "xyzzy")
    self.assertEqual(old_baz, "")

    config.set(self.db, "baz", "asdasd")
    new_baz = config.get(self.db, "baz")
    self.assertEqual(new_baz, "asdasd")
