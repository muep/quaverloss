# tests/test_instruments.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import unittest

import quaverloss.db.instruments as instruments
import quaverloss.dbhelp as qdb

class InstrumentsTest(unittest.TestCase):
  def memdb(self):
        return qdb.memdb()

  def setUp(self):
    self.db = self.memdb()

    instruments.add(self.db, "guitar")
    instruments.add(self.db, "whistle")

    self.whistle_id = instruments.resolve_id(self.db, "whistle")

    instruments.add(self.db, "foo")
    self.invalid_id = instruments.resolve_id(self.db, "foo")
    instruments.rm(self.db, self.invalid_id)

  def tearDown(self):
    self.db.close()

  # A Nice array of tests for different name-to-id mapping cases.
  def test_id_resolve_none(self):
    instId = instruments.resolve_id(self.db, None)
    self.assertIsNone(instId)

  def test_resolve_id_by_name_ok(self):
    instId = instruments.resolve_id(self.db, "whistle")
    self.assertEqual(instId, self.whistle_id)

  def test_resolve_id_by_id_ok(self):
    instId = instruments.resolve_id(self.db, self.whistle_id)
    self.assertEqual(instId, self.whistle_id)

  def test_resolve_id_by_name_fail(self):
    def stuff():
      instruments.resolve_id(self.db, "flute")
    self.assertRaises(qdb.IdResolutionError, stuff)

  def test_resolve_id_by_id_fail(self):
    def stuff():
      instruments.resolve_id(self.db, self.invalid_id)
    self.assertRaises(qdb.IdResolutionError, stuff)


  def test_add(self):
    def find_flute():
      return instruments.resolve_id(self.db, "flute")

    def add_flute():
      instruments.add(self.db, "flute")

    self.assertRaises(qdb.IdResolutionError, find_flute)
    add_flute()
    flute_id = find_flute()

    self.assertRaises(Exception, add_flute)

  def test_rm(self):
    def find_whistle():
      return instruments.resolve_id(self.db, "whistle")

    def rm_whistle():
      instruments.rm(self.db, "whistle")

    whistle_id = find_whistle()
    rm_whistle()
    self.assertRaises(Exception, rm_whistle)
    self.assertRaises(qdb.IdResolutionError, find_whistle)

  def test_rename(self):
    def find_whistle():
      return instruments.resolve_id(self.db, "whistle")

    def find_flute():
      return instruments.resolve_id(self.db, "flute")

    self.assertRaises(qdb.IdResolutionError, find_flute)
    whistle_id = find_whistle()

    instruments.set_fields(self.db, whistle_id, { "name" : "flute" } )

    self.assertRaises(qdb.IdResolutionError, find_whistle)
    flute_id = find_flute()

    self.assertEqual(whistle_id, flute_id)

  def test_by_name(self):
    whistle_details = instruments.by_name(self.db, self.whistle_id)
    self.assertEqual(whistle_details["name"], "whistle")

  def test_get_all(self):
    all_insts = instruments.get_all(self.db)

    # We get a list of dictionaries as a result
    names = list(map(lambda d: d["instrument_name"], all_insts))
    self.assertEqual(len(names), 2)
    self.assertIn("whistle", names)
    self.assertIn("guitar", names)

class InstrumentsTest_schema_v1(InstrumentsTest):
    def memdb(self):
        return qdb.memdb(schema_version=1)
