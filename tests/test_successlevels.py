# tests/test_successlevels.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import unittest

import quaverloss.db.successlevels as slevels
import quaverloss.dbhelp as qdb

class SuccessLevelsTests(unittest.TestCase):
  def memdb(self):
        return qdb.memdb()

  def setUp(self):
    self.db = self.memdb()

    slevels.add(self.db, "bad", 50, "")
    slevels.add(self.db, "ok", 100, "")

    self.bad_id = slevels.resolve_id(self.db, "bad")
    self.ok_id  = slevels.resolve_id(self.db, "ok")

    slevels.add(self.db, "good", 1000, "")
    self.invalid_id = slevels.resolve_id(self.db, "good")
    slevels.rm(self.db, "good")

  def tearDown(self):
    self.db.close()

  # A Nice array of tests for different name-to-id mapping cases.
  def test_id_resolve_none(self):
    slId = slevels.resolve_id(self.db, None)
    self.assertIsNone(slId)

  def test_resolve_id_by_name_ok(self):
    slId = slevels.resolve_id(self.db, "ok")
    self.assertEqual(slId, self.ok_id)

  def test_resolve_id_by_id_ok(self):
    slId = slevels.resolve_id(self.db, self.bad_id)
    self.assertEqual(slId, self.bad_id)

  def test_resolve_id_by_name_fail(self):
    def stuff():
      slevels.resolve_id(self.db, "good")
    self.assertRaises(qdb.IdResolutionError, stuff)

  def test_resolve_id_by_id_fail(self):
    def stuff():
      slevels.resolve_id(self.db, self.invalid_id)
    self.assertRaises(qdb.IdResolutionError, stuff)

  def test_add(self):
    def find_silly():
      return slevels.resolve_id(self.db, "silly")

    self.assertRaises(qdb.IdResolutionError, find_silly)

    slevels.add(self.db, "silly", 30, "")
    silly_id = find_silly()

  def test_rm(self):
    def find_bad():
      return slevels.resolve_id(self.db, "bad")

    bad_id = find_bad()
    slevels.rm(self.db, "bad")

    self.assertRaises(qdb.IdResolutionError, find_bad)

  def test_by_name(self):
    bad = slevels.by_name(self.db, "bad")
    self.assertEqual(bad["name"], "bad")
    self.assertEqual(bad["niceness"], 50)
    self.assertEqual(bad["description"], "")

  def test_get_all(self):
    lvls = slevels.get_all(self.db)

    self.assertEqual(len(lvls), 2)

  def test_rename(self):
    before = slevels.by_name(self.db, "bad")
    slevels.set_fields(self.db, "bad", { "name" : "silly" })
    after = slevels.by_name(self.db, "silly")
    self.assertEqual(before["name"], "bad")
    self.assertEqual(before["niceness"], 50)
    self.assertEqual(after["name"], "silly")
    self.assertEqual(after["niceness"], 50)

  def test_renice(self):
    before = slevels.by_name(self.db, "bad")
    self.assertEqual(before["niceness"], 50)
    slevels.set_fields(self.db, "bad", { "niceness" : 20 })
    after = slevels.by_name(self.db, "bad")
    self.assertEqual(after["niceness"], 20)

  def test_redesc(self):
    before = slevels.by_name(self.db, "bad")
    self.assertEqual(before["description"], "")
    slevels.set_fields(self.db, "bad", { "description" : 20 })
    after = slevels.by_name(self.db, "bad")
    self.assertEqual(after["description"], "20")

class SuccessLevelsTests_schema_v1(SuccessLevelsTests):
    def memdb(self):
        return qdb.memdb(schema_version=1)
