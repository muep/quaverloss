# tests/test_tunes.py
# coding: utf-8
#
# Copyright 2013 Joonas Sarajärvi
#
# This file is part of Quaverloss.
#
# Quaverloss is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quaverloss is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Quaverloss. In source copies of Quaverloss, it is
# usually included in files named COPYING and COPYING.LESSER. The
# license is also available at <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import unittest

import quaverloss.dbhelp as qdb
import quaverloss.db.maintain as maintain
import quaverloss.db.tunes as tunes

class TunesTests(unittest.TestCase):
  def memdb(self):
        return qdb.memdb()

  def setUp(self):
    self.db = self.memdb()
    tunes.add(self.db, "foo", uris=["asdasd"])
    tunes.add(self.db, "bar")

    # Find out a certainly valid tune id
    bar_dets = tunes.details(self.db, "bar")
    self.bar_tune_id = bar_dets["tune_id"]

    # Attempt to produce a tune id that is certainly not valid.
    tunes.add(self.db, "unexistent")
    udets = tunes.details(self.db, "unexistent")
    self.invalid_tune_id = udets["tune_id"]
    tunes.rm(self.db, "unexistent")

  def tearDown(self):
    self.db.close()

  # A Nice array of tests for different name-to-tuneid mapping cases.
  def test_id_resolve_none(self):
    tuneId = tunes.resolve_id(self.db, None)
    self.assertIsNone(tuneId)

  def test_resolve_id_by_name_ok(self):
    tuneId = tunes.resolve_id(self.db, "bar")
    self.assertEqual(tuneId, self.bar_tune_id)

  def test_resolve_id_by_id_ok(self):
    tuneId = tunes.resolve_id(self.db, self.bar_tune_id)
    self.assertEqual(tuneId, self.bar_tune_id)

  def test_resolve_id_by_name_fail(self):
    def stuff():
      tunes.resolve_id(self.db, "baz")
    self.assertRaises(qdb.IdResolutionError, stuff)

  def test_resolve_id_by_id_fail(self):
    def stuff():
      tunes.resolve_id(self.db, self.invalid_tune_id)
    self.assertRaises(qdb.IdResolutionError, stuff)

  def test_add(self):
    tunes.add(self.db, "baz")
    bazId = tunes.resolve_id(self.db, "baz")

  def test_add_dupes(self):
    def stuff():
      tunes.add(self.db, "baz")
    stuff()

    self.assertRaises(Exception, stuff)

  def test_rm(self):
    def stuff():
      return tunes.resolve_id(self.db, "foo")

    fooId = stuff()

    tunes.rm(self.db, "foo")
    self.assertRaises(qdb.IdResolutionError, stuff)


  def test_details_by_name(self):
    dets = tunes.details(self.db, "foo")

    self.assertEqual(dets["name"], "foo")
    self.assertEqual(dets["uris"], ["asdasd"])

  def test_details_by_id(self):
    dets = tunes.details(self.db, self.bar_tune_id)
    self.assertEqual(dets["tune_id"], self.bar_tune_id)
    self.assertEqual(dets["name"], "bar")

  def test_details_missing_by_name(self):
    def stuff():
      tunes.details(self.db, "baz")

    self.assertRaises(Exception, stuff)

  def test_details_missing_by_id(self):
    def stuff():
      tunes.details(self.db, self.invalid_tune_id)

  def test_search(self):
    def pick_name(tune):
      return tune["tune_name"]

    # An empty search is supposed to match all tunes
    allTunes = list(map(pick_name, tunes.search_by_name(self.db, "")))
    self.assertEqual(2, len(allTunes))
    self.assertIn("foo", allTunes)
    self.assertIn("bar", allTunes)


  def test_search_2(self):
    def pick_name(tune):
      return tune["tune_name"]

    # Tunes with an "o" somewhere in them
    names = list(map(pick_name, tunes.search_by_name(self.db, "o")))
    self.assertEqual(names, ["foo"])

    # Tunes that end with "fo"
    names = list(map(pick_name, tunes.search_by_name(self.db, "fo$")))
    self.assertEqual(names, [])

    # Tunes that start with "fo"
    names = list(map(pick_name, tunes.search_by_name(self.db, "^fo")))
    self.assertEqual(names, ["foo"])

    # Tunes that are exactly "fo"
    names = list(map(pick_name, tunes.search_by_name(self.db, "^fo$")))
    self.assertEqual(names, [])

    # Tunes that are exactly "foo"
    names = list(map(pick_name, tunes.search_by_name(self.db, "foo")))
    self.assertEqual(names, ["foo"])

  def test_set_name(self):
    def find_bar():
      return tunes.resolve_id(self.db, "bar")

    def find_baz():
      return tunes.resolve_id(self.db, "baz")

    barId = find_bar()
    self.assertRaises(qdb.IdResolutionError, find_baz)

    tunes.set_fields(self.db, self.bar_tune_id, {"name": "baz"})

    bazId = find_baz()
    self.assertEqual(barId, bazId)
    self.assertRaises(qdb.IdResolutionError, find_bar)

  def test_set_tsuri(self):
    before = tunes.details(self.db, "bar")
    self.assertEqual(before["uris"], [])
    tunes.set_fields(self.db, "bar", {"uris": ["xyzzy"]} )
    after = tunes.details(self.db, "bar")
    self.assertEqual(after["uris"], ["xyzzy"])

  def test_add_uri(self):
    before = tunes.details(self.db, "bar")
    self.assertEqual([], before["uris"])

    tunes.add_uri(self.db, "bar", "test://bar")

    after = tunes.details(self.db, "bar")
    self.assertEqual(["test://bar"], after["uris"])

    if maintain.schema_version_in_db(self.db) >= 2:
      tunes.add_uri(self.db, "bar", "foo://bar")

      yet_after = tunes.details(self.db, "bar")
      self.assertEqual({"test://bar", "foo://bar"}, set(yet_after["uris"]))
    else:
      def add_one():
        tunes.add_uri(self.db, "bar", "foo://bar")
      self.assertRaises(Exception, add_one)

  def test_rm_uri(self):
    before = tunes.details(self.db, "foo")
    self.assertEqual(set(before["uris"]), {"asdasd"})

    # Let's remove an URI that does not exist
    tunes.rm_uri(self.db, "foo", "lolapua")
    still = tunes.details(self.db, "foo")
    self.assertEqual(set(still["uris"]), {"asdasd"})

    tunes.add_uri(self.db, "bar", "asdasd")

    # Remove for real
    tunes.rm_uri(self.db, "foo", "asdasd")
    foo_after = tunes.details(self.db, "foo")
    bar_after = tunes.details(self.db, "bar")
    self.assertEqual(set(foo_after["uris"]), set())
    self.assertEqual(set(bar_after["uris"]), {"asdasd"})

class TunesTests_schema_v1(TunesTests):
    def memdb(self):
        return qdb.memdb(schema_version=1)
