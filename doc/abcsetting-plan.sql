-- abcsettings-plan.sql
--
-- Another currently unused piece of SQL table layout.

DROP TABLE IF EXISTS abcSetting;

-- There are often many ways a tune is played, An arbitrary number of
-- ABC notations can be stored for each tune.
CREATE TABLE abcSetting (
  abcSettingId INTEGER PRIMARY KEY UNIQUE,
  tuneId INTEGER NOT NULL REFERENCES tune(tuneId) ON DELETE CASCADE,
  abc TEXT NOT NULL
);
