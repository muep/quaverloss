-- comment-plan.sql
--
-- A planned structure for storing comments in the quaverloss
-- database. Comments could be related to many kinds of data.

CREATE TABLE comment (
  commentId INTEGER PRIMARY KEY UNIQUE,
  addStamp INTEGER NOT NULL,
  title TEXT NOT NULL,
  body TEXT NOT NULL
);

CREATE TABLE commentTuneMap (
  commentTuneMapId INTEGER PRIMARY KEY UNIQUE,
  commentId INTEGER NOT NULL
    -- Deleting a comment will also delete this relationship
    REFERENCES comment(commentId) ON DELETE CASCADE,
  tuneId INTEGER NOT NULL
    -- But deleting a tune will not be possible if comments are
    -- related to it.
    REFERENCES tune(tuneId) ON DELETE CASCADE
);

CREATE TABLE commentPracticeSessionMap (
  commentPracticeSessionMapId INTEGER PRIMARY KEY UNIQUE,
  commentId INTEGER NOT NULL
    -- Deleting a comment will also delete this relationship
    REFERENCES comment(commentId) ON DELETE CASCADE,
  practiceSessionId INTEGER NOT NULL
    REFERENCES practiceSession(practiceSessionId) ON DELETE CASCADE
);
