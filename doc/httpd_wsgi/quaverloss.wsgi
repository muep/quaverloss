# for_apache.wsgi
#
# A module that just simplifies things a bit so that the
# web API module can be used with mod_wsgi for the Apache httpd.

import quaverloss.web.api as api

# mod_wsgi seems to have been hard-coded to look for an object that is
# specifically named application.
#
# We hard-code the module to read a config from a file if such is
# provided.
application = api.wsgi_application(api.config_dict("/etc/quaverloss_web.conf"))
