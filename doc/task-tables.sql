-- task-tables.sql
--
-- Tables for storing tasks in the Quaverloss database. The current
-- implementation does not support using them well, so they were moved
-- here for now.

DROP TABLE IF EXISTS task;
DROP TABLE IF EXISTS taskTuneMap;
DROP TABLE IF EXISTS taskInstrumentMap;

-- For marking things the user should practice on
CREATE TABLE task (
  taskId INTEGER PRIMARY KEY UNIQUE,
  name TEXT NOT NULL UNIQUE,
  addStamp INTEGER NOT NULL,
  description TEXT NOT NULL DEFAULT '',
  completionStamp INTEGER DEFAULT NULL
);

-- For associating tasks with specific tunes. Likely just one tune
-- per task.
CREATE TABLE taskTuneMap (
  taskTuneMapId INTEGER PRIMARY KEY UNIQUE,
  tuneId INTEGER NOT NULL
    REFERENCES tune(tuneId) ON DELETE RESTRICT,
  taskId INTEGER NOT NULL
    REFERENCES task(taskId) ON DELETE RESTRICT
);

-- For associating tasks with specific instruments. Most of the smaller
-- learning tasks are believed to be somewhat instrument specific.
-- There will likely be just one entry per task here.
CREATE TABLE taskInstrumentMap (
  taskInstrumentMapId INTEGER PRIMARY KEY UNIQUE,
  instrumentId INTEGER NOT NULL
    REFERENCES instrument(instrumentId) ON DELETE RESTRICT,
  taskId INTEGER NOT NULL
    REFERENCES task(taskId) ON DELETE RESTRICT
);
